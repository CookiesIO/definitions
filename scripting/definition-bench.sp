#pragma dynamic 131072

#include <sourcemod>
#include <definitions>


public void OnAllPluginsLoaded()
{
    const int createIterations = 5000;
    const int readIterations = 1000000;

    float startTime = 0.0;
    Definition definition = null;
    DefinitionCreator creator = null;
    char createNames[createIterations][7];

    PrintToServer("} Preparing benchmark tests");

    for (int i = 0; i < createIterations; i++)
    {
        IntToString(i, createNames[i], sizeof(createNames[]));
    }

    PrintToServer("} Starting creation benchmark");
    startTime = GetEngineTime();

    for (int i = 0; i < createIterations; i++)
    {
        creator = new DefinitionCreator(createNames[i]);
        definition = creator.Finalize();
    }

    PrintToServer("} Created %d definitions in %f second(s)", createIterations, GetEngineTime() - startTime);



    PrintToServer("} Starting cleanup benchmark");
    startTime = GetEngineTime();

    DestroyMyDefinitions();

    PrintToServer("} Destroyed %d definitions in %f second(s)", createIterations, GetEngineTime() - startTime);


    definition = null;


    PrintToServer("} Starting creation benchmark, with parent");
    startTime = GetEngineTime();

    for (int i = 0; i < createIterations; i++)
    {
        creator = new DefinitionCreator(createNames[i], definition);
        definition = creator.Finalize();
    }

    PrintToServer("} Created %d definitions with parents in %f second(s)", createIterations, GetEngineTime() - startTime);


    int num;
    char myString[256];
    for (int i = 0; i < 256; i++)
    {
        myString[i] = 'a';
    }
    myString[255] = '\0';
    creator = new DefinitionCreator("ReadInt");
    creator.SetInt("TestInt", 1);
    creator.SetString("TestString", myString);
    creator.SetFunction("TestFunction", MyFunction);
    definition = creator.Finalize();

    
    PrintToServer("} Starting reading int benchmark");
    startTime = GetEngineTime();

    for (int i = 0; i < readIterations; i++)
    {
        definition.GetInt("TestInt", num);
    }

    PrintToServer("} Read %d ints in %f second(s)", readIterations, GetEngineTime() - startTime);


    
    PrintToServer("} Starting reading string benchmark");
    startTime = GetEngineTime();

    for (int i = 0; i < readIterations; i++)
    {
        definition.GetString("TestString", myString, sizeof(myString));
    }

    PrintToServer("} Read %d strings in %f second(s)", readIterations, GetEngineTime() - startTime);


    
    PrintToServer("} Starting StartFunction benchmark");
    startTime = GetEngineTime();

    for (int i = 0; i < readIterations; i++)
    {
        definition.StartFunction("TestFunction");
    }

    PrintToServer("} Started %d functions in %f second(s)", readIterations, GetEngineTime() - startTime);

    DestroyMyDefinitions();
}

void MyFunction()
{

}