#if defined _DEFINITIONS_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_INCLUDED

methodmap Definition < Handle
{
	/**
	 * Property to get the lenght of the name, useful when needing to retrieve the name of the definition
	 **/
	property int NameLength
	{
		public native get();
	}

	/**
	 * Property to get the parent definition
	 **/
	property Definition Parent
	{
		public native get();
	}

	/**
	 * Property to get whether the definition is a template or not
	 **/
	property bool IsTemplate
	{
		public native get();
	}

	/**
	 * Property to get whether the definition is completed or not, meaning all references are resolved
	 **/
	property bool IsComplete
	{
		public native get();
	}

	/**
	 * Gets the name of the definition
	 **/
	public native void GetName(char[] name, int max_size);

	/**
	 * Compares the input name with the definitions name to see if it (should) be the definition you're looking for
	 **/
	public bool IsDefinition(const char[] name)
	{
		int nameLength = this.NameLength + 1;
		char[] definitionName = new char[nameLength];
		this.GetName(definitionName, nameLength);
		if (strcmp(name, definitionName, false) == 0)
		{
			return true;
		}
		return false;
	}

	public bool IsDescendantOf(Definition definition, bool direct=false)
	{
		return IsDescendantOf(this, definition, direct);
	}

	public bool IsValid(bool allowTemplate=true)
	{
		return (this != null && (allowTemplate || this.IsTemplate == false));
	}

	public bool IsValidEx(bool allowTemplate, Definition parent, bool direct=false)
	{
		return (this.IsValid(allowTemplate) && this.IsDescendantOf(parent, direct));
	}

	/**
	 * Gets whether the definition originates from the plugin or not
	 **/
	public native bool IsFromPlugin(Handle plugin, bool checkParents=false);

	/**
	 * Gets an array of the definitions children
	 * Don't forget to delete the array after use!
	 **/
	public native ArrayList GetChildren(bool directDescendants=false, bool filterTemplates=true);

	/**
	 * Gets a property from the definition
	 **/
	public native bool GetInt(const char[] property, int &value);
	public native bool GetBool(const char[] property, bool &value);
	public native bool GetCell(const char[] property, any &value);
	public native bool GetFloat(const char[] property, float &value);
	public native bool GetStringLength(const char[] property, int &length);
	public native bool GetString(const char[] property, char[] value, int length);
	public native bool GetDefinition(const char[] property, Definition &value);
	public native bool GetArrayInt(const char[] property, int index, int &value);
	public native bool GetArrayBool(const char[] property, int index, bool &value);
	public native bool GetArrayCell(const char[] property, int index, any &value);
	public native bool GetArrayFloat(const char[] property, int index, float &value);
	public native bool GetArrayString(const char[] property, int index, char[] value, int length);
	public native bool GetArrayDefinition(const char[] property, int index, Definition &value);
	public native bool GetIntArrayLength(const char[] property, int &length);
	public native bool GetBoolArrayLength(const char[] property, int &length);
	public native bool GetCellArrayLength(const char[] property, int &length);
	public native bool GetFloatArrayLength(const char[] property, int &length);
	public native bool GetStringArrayLength(const char[] property, int &length, int &maxStringLength=0);
	public native bool GetDefinitionArrayLength(const char[] property, int &length);
	public native bool GetIntArray(const char[] property, int[] array, int length);
	public native bool GetBoolArray(const char[] property, bool[] array, int length);
	public native bool GetCellArray(const char[] property, any[] array, int length);
	public native bool GetFloatArray(const char[] property, float[] array, int length);
	public native bool GetDefinitionArray(const char[] property, Definition[] array, int length);

	public int GetIntEx(const char[] property, int defaultValue=0)
	{
		int value;
		if (this.GetInt(property, value))
		{
			return value;
		}
		return defaultValue;
	}

	public bool GetBoolEx(const char[] property, bool defaultValue=false)
	{
		bool value;
		if (this.GetBool(property, value))
		{
			return value;
		}
		return defaultValue;
	}

	public any GetCellEx(const char[] property, any defaultValue=0)
	{
		any value;
		if (this.GetCell(property, value))
		{
			return value;
		}
		return defaultValue;
	}

	public float GetFloatEx(const char[] property, float defaultValue=0.0)
	{
		float value;
		if (this.GetFloat(property, value))
		{
			return value;
		}
		return defaultValue;
	}

	public void GetStringEx(const char[] property, char[] value, int length, const char[] defaultValue="")
	{
		if (this.GetString(property, value, length))
		{
			return;
		}
		strcopy(value, length, defaultValue);
	}

	public int GetStringLengthEx(const char[] property)
	{
		int value;
		if (this.GetStringLength(property, value))
		{
			return value;
		}
		return 0;
	}

	public Definition GetDefinitionEx(const char[] property, Definition defaultValue=null)
	{
		Definition value;
		if (this.GetDefinition(property, value))
		{
			return value;
		}
		return defaultValue;
	}

	public int GetArrayIntEx(const char[] property, int index, int defaultValue=0)
	{
		int value;
		if (this.GetArrayInt(property, index, value))
		{
			return value;
		}
		return defaultValue;
	}

	public bool GetArrayBoolEx(const char[] property, int index, bool defaultValue=false)
	{
		bool value;
		if (this.GetArrayBool(property, index, value))
		{
			return value;
		}
		return defaultValue;
	}

	public any GetArrayCellEx(const char[] property, int index, any defaultValue=0)
	{
		any value;
		if (this.GetArrayCell(property, index, value))
		{
			return value;
		}
		return defaultValue;
	}

	public float GetArrayFloatEx(const char[] property, int index, float defaultValue=0.0)
	{
		float value;
		if (this.GetArrayFloat(property, index, value))
		{
			return value;
		}
		return defaultValue;
	}

	public Definition GetArrayDefinitionEx(const char[] property, int index, Definition defaultValue=null)
	{
		Definition value;
		if (this.GetArrayDefinition(property, index, value))
		{
			return value;
		}
		return defaultValue;
	}

	public int GetIntArrayLengthEx(const char[] property)
	{
		int length;
		if (this.GetIntArrayLength(property, length))
		{
			return length;
		}
		return 0;
	}

	public int GetBoolArrayLengthEx(const char[] property)
	{
		int length;
		if (this.GetBoolArrayLength(property, length))
		{
			return length;
		}
		return 0;
	}

	public int GetCellArrayLengthEx(const char[] property)
	{
		int length;
		if (this.GetCellArrayLength(property, length))
		{
			return length;
		}
		return 0;
	}

	public int GetFloatArrayLengthEx(const char[] property)
	{
		int length;
		if (this.GetFloatArrayLength(property, length))
		{
			return length;
		}
		return 0;
	}

	public int GetStringArrayLengthEx(const char[] property)
	{
		int length;
		if (this.GetStringArrayLength(property, length))
		{
			return length;
		}
		return 0;
	}

	public int GetStringArrayStringMaxLength(const char[] property)
	{
		int length, maxLength;
		if (this.GetStringArrayLength(property, length, maxLength))
		{
			return maxLength;
		}
		return 0;
	}

	public int GetDefinitionArrayLengthEx(const char[] property)
	{
		int length;
		if (this.GetDefinitionArrayLength(property, length))
		{
			return length;
		}
		return 0;
	}

	/**
	 * Has a function set with this name
	 **/
	public native bool HasFunction(const char[] property);

	/**
	 * Start a function call for a function property
	 * Only use for infrequent calls
	 **/
	public native bool StartFunction(const char[] property);

	/**
	 * Adds the function in the property to a forward
	 * Use for frequent calls, for better performance
	 **/
	public native bool AddToForward(const char[] property, Handle fwd);

	public void StartFunctionEx(const char[] property, Function fallback)
	{
		if (!this.StartFunction(property))
		{
			Call_StartFunction(null, fallback);
		}
	}

	public void AddToForwardEx(const char[] property, Handle fwd, Function fallback)
	{
		if (!this.AddToForward(property, fwd))
		{
			AddToForward(fwd, null, fallback);
		}
	}

	/**
	 * Destroys the definition
	 * It wasn't planned to be added, but I decided to add it due the better compatibility with definitions based on configs.
	 *
	 * @error  Called by any plugin other than the owning plugin
	 **/
	public native void Destroy();
}

static bool IsDescendantOf(Definition definition, Definition other, bool direct)
{
	if (other == null || definition == null)
	{
		return false;
	}

	Definition parent = definition.Parent;
	if (parent != other)
	{
		if (direct == false)
		{
			return IsDescendantOf(parent, other, false);
		}
		return false;
	}
	return true;
}

/**
 * A definition validator is used to validate a child definition before it's brought public
 * If the child does not properly conform to your standards, it can be destroyed by returning false
 **/
typedef DefinitionValidator = function bool(Definition child);

methodmap DefinitionCreator < Handle
{
	/**
	 * Starts creation of a new definition, once started must be finalized, or it can leak memory.
	 * Make sure there's no problems while defining your definition!
	 * If any errors happens from one of the define or set functions in this methodmap, memory will be cleaned.
	 **/
	public native DefinitionCreator(const char[] name, Definition parent=null);

	/**
	 * Forces the definition to be abstract
	 **/
	public native void ForceTemplate();

	/**
	 * Defines/sets an integer property
	 **/
	public native void DefineInt(const char[] property, bool hasMin=false, int min=0, bool hasMax=false, int max=0);
	public native void SetInt(const char[] property, int value);

	/**
	 * Defines/sets a bool property
	 **/
	public native void DefineBool(const char[] property);
	public native void SetBool(const char[] property, bool value);

	/**
	 * Defines/sets a cell property
	 **/
	public native void DefineCell(const char[] property);
	public native void SetCell(const char[] property, any value);

	/**
	 * Defines/sets a float property
	 **/
	public native void DefineFloat(const char[] property, bool hasMin=false, float min=0.0, bool hasMax=false, float max=0.0);
	public native void SetFloat(const char[] property, float value);

	/**
	 * Defines/sets a string property
	 **/
	public native void DefineString(const char[] property);
	public native void SetString(const char[] property, const char[] value);

	/**
	 * Defines/sets a function property
	 **/
	public native void DefineFunction(const char[] property);
	public native void SetFunction(const char[] property, Function func);

	/**
	 * Defines/sets a reference property
	 **/
	public native void DefineDefinition(const char[] property, const char[] ancestor=NULL_STRING);
	public native void SetDefinition(const char[] property, const char[] value);

	/**
	 * Defines/clears/pushes a value to an int array property
	 * This type of array can be automatically created without being defined first.
	 **/
	public native void DefineIntArray(const char[] property);
	public native void ClearIntArray(const char[] property);
	public native void PushIntToArray(const char[] property, int value);

	/**
	 * Defines/clears/pushes a value to a bool array property
	 * This type of array can be automatically created without being defined first.
	 **/
	public native void DefineBoolArray(const char[] property);
	public native void ClearBoolArray(const char[] property);
	public native void PushBoolToArray(const char[] property, bool value);

	/**
	 * Defines/clears/pushes a value to an cell array property
	 * This type of array can be automatically created without being defined first.
	 **/
	public native void DefineCellArray(const char[] property);
	public native void ClearCellArray(const char[] property);
	public native void PushCellToArray(const char[] property, any value);

	/**
	 * Defines/clears/pushes a value to a float array property
	 * This type of array can be automatically created without being defined first.
	 **/
	public native void DefineFloatArray(const char[] property);
	public native void ClearFloatArray(const char[] property);
	public native void PushFloatToArray(const char[] property, float value);

	/**
	 * Defines/clears/pushes a value to a string array property
	 * This type of array can NOT be automatically created without being defined first, as it needs a string max length.
	 * Up for consideration whether or not this should use the same backing functionality of other Definition strings to allow variable length, which adds to the cost and complexity of use.
	 **/
	public native void DefineStringArray(const char[] property, int maxLength);
	public native void ClearStringArray(const char[] property);
	public native void PushStringToArray(const char[] property, const char[] value);

	/**
	 * Defines/clears/pushes a value to a reference array property
	 * This type of array can be automatically created without being defined first.
	 **/
	public native void DefineDefinitionArray(const char[] property, const char[] ancestor=NULL_STRING);
	public native void ClearDefinitionArray(const char[] property);
	public native void PushDefinitionToArray(const char[] property, const char[] value);

	/**
	 * Sets a validator to validate child definitions before they're publicly announced
	 **/
	public native void SetValidator(DefinitionValidator validator);

	/**
	 * Finalizes the definition, and registers it to the global definition lists
	 **/
	public native Definition Finalize();
}

/**
 * Finds a definitions by name
 **/
native Definition FindDefinition(const char[] name);

typeset DefinitionCreatedListener
{
	function void();
	function void(Definition definition);
}

typeset DefinitionDestroyedListener
{
	function void();
	function void(Definition definition);
}

/**
 * Starts listening to when a specific definition is created
 **/
native void AddDefinitionCreatedListener(const char[] name, DefinitionCreatedListener listener);

/**
 * Stops listening to when a specific definition is created
 **/
native void RemoveDefinitionCreatedListener(const char[] name, DefinitionCreatedListener listener);

/**
 * Starts listening to when a specific definition is destroyed
 **/
native void AddDefinitionDestroyedListener(const char[] name, DefinitionDestroyedListener listener);

/**
 * Stops listening to when a specific definition is destroyed
 **/
native void RemoveDefinitionDestroyedListener(const char[] name, DefinitionDestroyedListener listener);

/**
 * Called in all plugins when a new definition has been created
 **/
forward void OnDefinitionCreated(Definition definition);

/**
 * Called on owning plugin when it's definition is destroyed (to free eventual resources)
 * Called before Destroyed.
 **/
forward void OnDefinitionDestroy(Definition definition);

/**
 * Called in all plugins when a definition is destroyed, not all resources in the definition may be available at this time
 **/
forward void OnDefinitionDestroyed(Definition definition);

/**
 * IMPORTANT!
 * MUST be called in OnPluginEnd in order to clean up your definitions!!
 * Use sparingly otherwise, it's only recommended to use in OnPluginEnd
 * although it might also be useful for when definitions are loaded from configs.
 **/
native void DestroyMyDefinitions();

/* DO NOT EDIT BELOW THIS LINE */

public SharedPlugin __pl_definitions =
{
	name = "definitions",
	file = "definitions.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_definitions_SetNTVOptional()
{
	MarkNativeAsOptional("DestroyMyDefinitions");

	MarkNativeAsOptional("FindDefinition");
	MarkNativeAsOptional("AddDefinitionCreatedListener");
	MarkNativeAsOptional("RemoveDefinitionCreatedListener");
	MarkNativeAsOptional("AddDefinitionDestroyedListener");
	MarkNativeAsOptional("RemoveDefinitionDestroyedListener");

	MarkNativeAsOptional("DefinitionCreator.DefinitionCreator");
	MarkNativeAsOptional("DefinitionCreator.ForceTemplate");
	MarkNativeAsOptional("DefinitionCreator.SetInt");
	MarkNativeAsOptional("DefinitionCreator.SetBool");
	MarkNativeAsOptional("DefinitionCreator.SetCell");
	MarkNativeAsOptional("DefinitionCreator.SetFloat");
	MarkNativeAsOptional("DefinitionCreator.SetString");
	MarkNativeAsOptional("DefinitionCreator.SetFunction");
	MarkNativeAsOptional("DefinitionCreator.SetDefinition");
	MarkNativeAsOptional("DefinitionCreator.ClearIntArray");
	MarkNativeAsOptional("DefinitionCreator.ClearBoolArray");
	MarkNativeAsOptional("DefinitionCreator.ClearCellArray");
	MarkNativeAsOptional("DefinitionCreator.ClearFloatArray");
	MarkNativeAsOptional("DefinitionCreator.ClearStringArray");
	MarkNativeAsOptional("DefinitionCreator.ClearDefinitionArray");
	MarkNativeAsOptional("DefinitionCreator.PushIntToArray");
	MarkNativeAsOptional("DefinitionCreator.PushBoolToArray");
	MarkNativeAsOptional("DefinitionCreator.PushCellToArray");
	MarkNativeAsOptional("DefinitionCreator.PushFloatToArray");
	MarkNativeAsOptional("DefinitionCreator.PushStringToArray");
	MarkNativeAsOptional("DefinitionCreator.PushDefinitionToArray");
	MarkNativeAsOptional("DefinitionCreator.DefineInt");
	MarkNativeAsOptional("DefinitionCreator.DefineBool");
	MarkNativeAsOptional("DefinitionCreator.DefineCell");
	MarkNativeAsOptional("DefinitionCreator.DefineFloat");
	MarkNativeAsOptional("DefinitionCreator.DefineString");
	MarkNativeAsOptional("DefinitionCreator.DefineFunction");
	MarkNativeAsOptional("DefinitionCreator.DefineDefinition");
	MarkNativeAsOptional("DefinitionCreator.DefineIntArray");
	MarkNativeAsOptional("DefinitionCreator.DefineBoolArray");
	MarkNativeAsOptional("DefinitionCreator.DefineCellArray");
	MarkNativeAsOptional("DefinitionCreator.DefineFloatArray");
	MarkNativeAsOptional("DefinitionCreator.DefineStringArray");
	MarkNativeAsOptional("DefinitionCreator.DefineDefinitionArray");
	MarkNativeAsOptional("DefinitionCreator.SetValidator");
	MarkNativeAsOptional("DefinitionCreator.Finalize");

	MarkNativeAsOptional("Definition.NameLength.get");
	MarkNativeAsOptional("Definition.GetName");
	MarkNativeAsOptional("Definition.Parent.get");
	MarkNativeAsOptional("Definition.IsTemplate.get");
	MarkNativeAsOptional("Definition.IsComplete.get");
	MarkNativeAsOptional("Definition.IsFromPlugin");
	MarkNativeAsOptional("Definition.GetChildren");
	MarkNativeAsOptional("Definition.GetInt");
	MarkNativeAsOptional("Definition.GetBool");
	MarkNativeAsOptional("Definition.GetCell");
	MarkNativeAsOptional("Definition.GetFloat");
	MarkNativeAsOptional("Definition.GetString");
	MarkNativeAsOptional("Definition.HasFunction");
	MarkNativeAsOptional("Definition.StartFunction");
	MarkNativeAsOptional("Definition.AddToForward");
	MarkNativeAsOptional("Definition.GetDefinition");
	MarkNativeAsOptional("Definition.GetArrayInt");
	MarkNativeAsOptional("Definition.GetArrayBool");
	MarkNativeAsOptional("Definition.GetArrayCell");
	MarkNativeAsOptional("Definition.GetArrayFloat");
	MarkNativeAsOptional("Definition.GetArrayString");
	MarkNativeAsOptional("Definition.GetArrayDefinition");
	MarkNativeAsOptional("Definition.GetIntArray");
	MarkNativeAsOptional("Definition.GetBoolArray");
	MarkNativeAsOptional("Definition.GetCellArray");
	MarkNativeAsOptional("Definition.GetFloatArray");
	MarkNativeAsOptional("Definition.GetDefinitionArray");
	MarkNativeAsOptional("Definition.GetIntArrayLength");
	MarkNativeAsOptional("Definition.GetBoolArrayLength");
	MarkNativeAsOptional("Definition.GetCellArrayLength");
	MarkNativeAsOptional("Definition.GetFloatArrayLength");
	MarkNativeAsOptional("Definition.GetStringArrayLength");
	MarkNativeAsOptional("Definition.GetDefinitionArrayLength");
	MarkNativeAsOptional("Definition.Destroy");
}
#endif