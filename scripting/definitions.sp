#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>

// Include order matters.
#include "definitions/dstring.sp"
#include "definitions/function-table.sp"
#include "definitions/dreference.sp"
#include "definitions/internal-definition.sp"
#include "definitions/management.sp"
#include "definitions/utilities.sp"

#include "definitions/properties/int.sp"
#include "definitions/properties/bool.sp"
#include "definitions/properties/cell.sp"
#include "definitions/properties/float.sp"
#include "definitions/properties/string.sp"
#include "definitions/properties/function.sp"
#include "definitions/properties/reference.sp"
#include "definitions/properties/array.sp"

/*********************************************************************
 *
 *  Definitions was made to fulfil a generalized purpose of an idea
 *  I had in my old version of Gamma. Which was Behavior Types and
 *  Behaviors. Definitions mix both of them together into one and
 *  does it in a way that fulfils many inter-plugin extension needs.
 *
 *  What definitions does, is that it allows one plugin to define
 *  a set of properties that it needs in order to function, and then
 *  any other plugin can either set or even extend on those
 *  properties. This even allows plugins to create deifnitions that
 *  doesn't exactly provide anything with functionality to the
 *  original plugin, but instead increases functionality to other
 *  plugins that may wish to depend on the plugins functionality.
 *
 *  An example could be, in this case, Gamma the Game Mode Manager,
 *  and Boss Fight Fortress.
 *  How come? Well, you see, for Boss Fight Fortress there's already
 *  written a lot of code to handle a Boss game mode, so there's
 *  really no reason to actually maintain 2 code bases for the two
 *  variations: Boss vs All and Bosses vs Bosses.
 *
 *  Instead there can be a base plugin: Boss Fight Fortress - Base
 *  and two sub plugins: Boss vs All and Bosses vs Bosses.
 *
 *  Boss vs All would make it so that a single client is chosen as
 *  the boss, while Bosses vs Bosses would make all clients bosses.
 *  To fix bugs in both variations, the old change needed would be
 *  to the base plugin! That is, if it's bugs in the base plugin :P
 *
 *********************************************************************/

#define PLUGIN_VERSION "1.0 alpha 3"

public Plugin myinfo =
{
	name = "Definitions",
	author = "Cookies.io",
	description = "Manages definitions across of plugins, highly useful in plugins needing to be extended by other plugins",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public APLRes AskPluginLoad2()
{
	CreateNative("DestroyMyDefinitions", Native_DestroyMyDefinitions);

	CreateNative("FindDefinition", Native_FindDefinition);
	CreateNative("AddDefinitionCreatedListener", Native_AddDefinitionCreatedListener);
	CreateNative("RemoveDefinitionCreatedListener", Native_RemoveDefinitionCreatedListener);
	CreateNative("AddDefinitionDestroyedListener", Native_AddDefinitionDestroyedListener);
	CreateNative("RemoveDefinitionDestroyedListener", Native_RemoveDefinitionDestroyedListener);

	CreateNative("DefinitionCreator.DefinitionCreator", Native_StartCreateDefinition);
	CreateNative("DefinitionCreator.ForceTemplate", Native_ForceTemplate);
	CreateNative("DefinitionCreator.SetInt", Native_SetDefinitionInt);
	CreateNative("DefinitionCreator.SetBool", Native_SetDefinitionBool);
	CreateNative("DefinitionCreator.SetCell", Native_SetDefinitionCell);
	CreateNative("DefinitionCreator.SetFloat", Native_SetDefinitionFloat);
	CreateNative("DefinitionCreator.SetString", Native_SetDefinitionString);
	CreateNative("DefinitionCreator.SetFunction", Native_SetDefinitionFunction);
	CreateNative("DefinitionCreator.SetDefinition", Native_SetDefinitionDefinition);
	CreateNative("DefinitionCreator.ClearIntArray", Native_ClearDefinitionIntArray);
	CreateNative("DefinitionCreator.ClearBoolArray", Native_ClearDefinitionBoolArray);
	CreateNative("DefinitionCreator.ClearCellArray", Native_ClearDefinitionCellArray);
	CreateNative("DefinitionCreator.ClearFloatArray", Native_ClearDefinitionFloatArray);
	CreateNative("DefinitionCreator.ClearStringArray", Native_ClearDefinitionStringArray);
	CreateNative("DefinitionCreator.ClearDefinitionArray", Native_ClearDefinitionDefinitionArray);
	CreateNative("DefinitionCreator.PushIntToArray", Native_PushIntToDefinitionArray);
	CreateNative("DefinitionCreator.PushBoolToArray", Native_PushBoolToDefinitionArray);
	CreateNative("DefinitionCreator.PushCellToArray", Native_PushCellToDefinitionArray);
	CreateNative("DefinitionCreator.PushFloatToArray", Native_PushFloatToDefinitionArray);
	CreateNative("DefinitionCreator.PushStringToArray", Native_PushStringToDefinitionArray);
	CreateNative("DefinitionCreator.PushDefinitionToArray", Native_PushDefinitionToDefinitionArray);
	CreateNative("DefinitionCreator.DefineInt", Native_DefineDefinitionInt);
	CreateNative("DefinitionCreator.DefineBool", Native_DefineDefinitionBool);
	CreateNative("DefinitionCreator.DefineCell", Native_DefineDefinitionCell);
	CreateNative("DefinitionCreator.DefineFloat", Native_DefineDefinitionFloat);
	CreateNative("DefinitionCreator.DefineString", Native_DefineDefinitionString);
	CreateNative("DefinitionCreator.DefineFunction", Native_DefineDefinitionFunction);
	CreateNative("DefinitionCreator.DefineDefinition", Native_DefineDefinitionDefinition);
	CreateNative("DefinitionCreator.DefineIntArray", Native_DefineDefinitionIntArray);
	CreateNative("DefinitionCreator.DefineBoolArray", Native_DefineDefinitionBoolArray);
	CreateNative("DefinitionCreator.DefineCellArray", Native_DefineDefinitionCellArray);
	CreateNative("DefinitionCreator.DefineFloatArray", Native_DefineDefinitionFloatArray);
	CreateNative("DefinitionCreator.DefineStringArray", Native_DefineDefinitionStringArray);
	CreateNative("DefinitionCreator.DefineDefinitionArray", Native_DefineDefinitionDefinitionArray);
	CreateNative("DefinitionCreator.SetValidator", Native_SetDefinitionValidator);
	CreateNative("DefinitionCreator.Finalize", Native_FinalizeCreateDefinition);

	CreateNative("Definition.NameLength.get", Native_GetDefinitionNameLength);
	CreateNative("Definition.GetName", Native_GetDefinitionName);
	CreateNative("Definition.Parent.get", Native_GetDefinitionParent);
	CreateNative("Definition.IsTemplate.get", Native_GetIsTemplate);
	CreateNative("Definition.IsComplete.get", Native_GetIsComplete);
	CreateNative("Definition.IsFromPlugin", Native_IsDefinitionFromPlugin);
	CreateNative("Definition.GetChildren", Native_GetDefinitionChildren);
	CreateNative("Definition.GetInt", Native_GetDefinitionInt);
	CreateNative("Definition.GetBool", Native_GetDefinitionBool);
	CreateNative("Definition.GetCell", Native_GetDefinitionCell);
	CreateNative("Definition.GetFloat", Native_GetDefinitionFloat);
	CreateNative("Definition.GetString", Native_GetDefinitionString);
	CreateNative("Definition.HasFunction", Native_HasDefinitionFunction);
	CreateNative("Definition.StartFunction", Native_StartDefinitionFunction);
	CreateNative("Definition.AddToForward", Native_AddDefinitionPropertyToForward);
	CreateNative("Definition.GetDefinition", Native_GetDefinitionDefinition);
	CreateNative("Definition.GetArrayInt", Native_GetDefinitionArrayInt);
	CreateNative("Definition.GetArrayBool", Native_GetDefinitionArrayBool);
	CreateNative("Definition.GetArrayCell", Native_GetDefinitionArrayCell);
	CreateNative("Definition.GetArrayFloat", Native_GetDefinitionArrayFloat);
	CreateNative("Definition.GetArrayString", Native_GetDefinitionArrayString);
	CreateNative("Definition.GetArrayDefinition", Native_GetDefinitionArrayDefinition);
	CreateNative("Definition.GetIntArray", Native_GetDefinitionIntArray);
	CreateNative("Definition.GetBoolArray", Native_GetDefinitionBoolArray);
	CreateNative("Definition.GetCellArray", Native_GetDefinitionCellArray);
	CreateNative("Definition.GetFloatArray", Native_GetDefinitionFloatArray);
	CreateNative("Definition.GetDefinitionArray", Native_GetDefinitionDefinitionArray);
	CreateNative("Definition.GetIntArrayLength", Native_GetDefinitionIntArrayLength);
	CreateNative("Definition.GetBoolArrayLength", Native_GetDefinitionBoolArrayLength);
	CreateNative("Definition.GetCellArrayLength", Native_GetDefinitionCellArrayLength);
	CreateNative("Definition.GetFloatArrayLength", Native_GetDefinitionFloatArrayLength);
	CreateNative("Definition.GetStringArrayLength", Native_GetDefinitionStringArrayLength);
	CreateNative("Definition.GetDefinitionArrayLength", Native_GetDefinitionDefinitionArrayLength);
	CreateNative("Definition.Destroy", Native_DefinitionDestroy);

	RegPluginLibrary("definitions");
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("definitions_version", PLUGIN_VERSION, "Definitions Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
	RegAdminCmd("definitions_list", ListDefinitionsCmd, ADMFLAG_ROOT, "Lists all definitions and their parents + state");

	SetupDefinitionManagement();
	SetupReferences();
}

public void OnPluginEnd()
{
	DestroyAllDefinitions();
}

// Unfinalized definitions shouldn't happen in general and only expected if there's an error thrown.
// But better cleaning them up, than them taking up space.
public void OnMapEnd()
{
	CleanupUnfinalizedDefinitions();
}

public Action ListDefinitionsCmd(int client, int args)
{
	PrintAllDefinitionsToClient(client);
}


public int Native_DestroyMyDefinitions(Handle plugin, int numParams)
{
	CleanupPluginDefinitions(plugin);
}


public int Native_FindDefinition(Handle plugin, int numParams)
{
	int length;
	GetNativeStringLength(1, length);

	length += 1;
	char[] name = new char[length];
	GetNativeString(1, name, length);

	return view_as<int>(FindDefinition(name));
}

public int Native_AddDefinitionCreatedListener(Handle plugin, int numParams)
{
	int length;
	GetNativeStringLength(1, length);

	length += 1;
	char[] name = new char[length];
	GetNativeString(1, name, length);

	Function func = GetNativeFunction(2);
	AddDefinitionCreatedListener(name, plugin, func);
	return 0;
}

public int Native_RemoveDefinitionCreatedListener(Handle plugin, int numParams)
{
	int length;
	GetNativeStringLength(1, length);

	length += 1;
	char[] name = new char[length];
	GetNativeString(1, name, length);

	Function func = GetNativeFunction(2);
	RemoveDefinitionCreatedListener(name, plugin, func);
	return 0;
}

public int Native_AddDefinitionDestroyedListener(Handle plugin, int numParams)
{
	int length;
	GetNativeStringLength(1, length);

	length += 1;
	char[] name = new char[length];
	GetNativeString(1, name, length);

	Function func = GetNativeFunction(2);
	AddDefinitionDestroyedListener(name, plugin, func);
	return 0;
}

public int Native_RemoveDefinitionDestroyedListener(Handle plugin, int numParams)
{
	int length;
	GetNativeStringLength(1, length);

	length += 1;
	char[] name = new char[length];
	GetNativeString(1, name, length);

	Function func = GetNativeFunction(2);
	RemoveDefinitionDestroyedListener(name, plugin, func);
	return 0;
}


public int Native_StartCreateDefinition(Handle plugin, int numParams)
{
	int nameLength;
	GetNativeStringLength(1, nameLength);

	if (nameLength < 1)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Definitions must have a name with 1 or more characters.");
		return 0;
	}

	nameLength += 1;
	char[] name = new char[nameLength];
	GetNativeString(1, name, nameLength);

	if (FindDefinition(name, DefinitionState_Unfinalized, false) != null)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Definition with name '%s' already exists.", name);
		return 0;
	}

	IDefinition definition = view_as<IDefinition>(GetNativeCell(2));
	if (definition != null && definition.IsFinalized == false)
	{
		int parentNameLength = definition.NameLength + 1;
		char[] parentName = new char[parentNameLength];
		definition.GetName(parentName, parentNameLength);
		ThrowNativeError(SP_ERROR_PARAM, "Parent definition with name '%s' was not finalized, cannot create child definition of it.", parentName);
		return 0;
	}
	return view_as<int>(new IDefinition(plugin, name, definition));
}

public int Native_ForceTemplate(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only force abstract from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to force abstract after the definition is finalized.");
		return 0;
	}

	definition.ForceTemplate();
	return true;
}

public int Native_SetDefinitionInt(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int value = GetNativeCell(3);

	if (Definition_SetPropertyInt(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than Int.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionBool(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	bool value = GetNativeCell(3);

	if (Definition_SetPropertyBool(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than Bool.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionCell(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	any value = GetNativeCell(3);

	if (Definition_SetPropertyCell(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than Cell.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionFloat(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	float value = GetNativeCell(3);

	if (Definition_SetPropertyFloat(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than Float.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionString(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);


	GetNativeStringLength(3, length);

	length += 1;
	char[] value = new char[length];
	GetNativeString(3, value, length);

	if (Definition_SetPropertyString(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than String.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionFunction(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	Function value = GetNativeFunction(3);

	if (Definition_SetPropertyFunction(definition, property, plugin, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than Function.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionDefinition(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only set definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	GetNativeStringLength(3, length);

	length += 1;
	char[] value = new char[length];
	GetNativeString(3, value, length);

	if (Definition_SetPropertyDefinition(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to set property '%s', a property with the same name has likely been defined to some other type than Definition.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}


public int Native_ClearDefinitionIntArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only clear definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_ClearPropertyArray(definition, property, PropertyType_Int) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear property '%s', the property either doesn't exist or there's likely a property with the same name defined to some other type than Int Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_ClearDefinitionBoolArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only clear definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_ClearPropertyArray(definition, property, PropertyType_Bool) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear property '%s', the property either doesn't exist or there's likely a property with the same name defined to some other type than Bool Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_ClearDefinitionCellArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only clear definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_ClearPropertyArray(definition, property, PropertyType_Cell) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear property '%s', the property either doesn't exist or there's likely a property with the same name defined to some other type than Cell Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_ClearDefinitionFloatArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only clear definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_ClearPropertyArray(definition, property, PropertyType_Float) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear property '%s', the property either doesn't exist or there's likely a property with the same name defined to some other type than Float Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_ClearDefinitionStringArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only clear definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_ClearPropertyArray(definition, property, PropertyType_String) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear property '%s', the property either doesn't exist or there's likely a property with the same name defined to some other type than String Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_ClearDefinitionDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only clear definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_ClearPropertyArray(definition, property, PropertyType_Definition) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to clear property '%s', the property either doesn't exist or there's likely a property with the same name defined to some other type than Definition Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}


public int Native_PushIntToDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only push to definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int value = GetNativeCell(3);

	if (Definition_PushToPropertyArray(definition, property, value, PropertyType_Int) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to property '%s', the property either doesn't exist or a property with the same name has likely been defined to some other type than Int Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_PushBoolToDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only push to definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	bool value = GetNativeCell(3);

	if (Definition_PushToPropertyArray(definition, property, value, PropertyType_Bool) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to property '%s', the property either doesn't exist or a property with the same name has likely been defined to some other type than Bool Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_PushCellToDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only push to definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	any value = GetNativeCell(3);

	if (Definition_PushToPropertyArray(definition, property, value, PropertyType_Cell) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to property '%s', the property either doesn't exist or a property with the same name has likely been defined to some other type than Cell Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_PushFloatToDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only push to definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	float value = GetNativeCell(3);

	if (Definition_PushToPropertyArray(definition, property, value, PropertyType_Float) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to property '%s', the property either doesn't exist or a property with the same name has likely been defined to some other type than Float Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_PushStringToDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only push to definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int valueLength;
	GetNativeStringLength(3, valueLength);
	
	valueLength += 1;
	char[] value = new char[valueLength];
	GetNativeString(3, value, valueLength);

	if (Definition_PushStringToPropertyArray(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to property '%s', the property either doesn't exist or a property with the same name has likely been defined to some other type than String Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_PushDefinitionToDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only push to definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int valueLength;
	GetNativeStringLength(3, valueLength);
	
	valueLength += 1;
	char[] value = new char[valueLength];
	GetNativeString(3, value, valueLength);

	if (Definition_PushDefinitionToPropertyArray(definition, property, value) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to push to property '%s', the property either doesn't exist or a property with the same name has likely been defined to some other type than String Array.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}


public int Native_DefineDefinitionInt(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	bool hasMin = GetNativeCell(3);
	int min = GetNativeCell(4);
	bool hasMax = GetNativeCell(5);
	int max = GetNativeCell(6);

	if (Definition_DefinePropertyInt(definition, property, _, hasMin, min, hasMax, max) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionBool(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyBool(definition, property) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionCell(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyCell(definition, property) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionFloat(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);
	
	bool hasMin = GetNativeCell(3);
	float min = GetNativeCell(4);
	bool hasMax = GetNativeCell(5);
	float max = GetNativeCell(6);

	if (Definition_DefinePropertyFloat(definition, property, _, hasMin, min, hasMax, max) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionString(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyString(definition, property) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionFunction(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyFunction(definition, property) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionDefinition(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	GetNativeStringLength(3, length);
	
	length += 1;
	char[] ancestor = new char[length];
	GetNativeString(3, ancestor, length);

	if (Definition_DefinePropertyDefinition(definition, property, ancestor) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionIntArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyArray(definition, property, PropertyType_Int) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionBoolArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyArray(definition, property, PropertyType_Bool) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionCellArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyArray(definition, property, PropertyType_Cell) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionFloatArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	if (Definition_DefinePropertyArray(definition, property, PropertyType_Float) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionStringArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int maxLength = GetNativeCell(3);

	if (Definition_DefinePropertyStringArray(definition, property, maxLength) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_DefineDefinitionDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define definition properties from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define properties after the definition is finalized.");
		return 0;
	}

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	GetNativeStringLength(3, length);

	length += 1;
	char[] ancestor = new char[length];
	GetNativeString(3, ancestor, length);

	if (Definition_DefinePropertyDefinitionArray(definition, property, ancestor) == false)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Unable to define property '%s', a property with the same name has likely already been defined.", property);
		definition.Destroy();
		return 0;
	}
	return true;
}

public int Native_SetDefinitionValidator(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Can only define set a definition validator from the owning plugin.");
		return 0;
	}

	if (definition.IsFinalized)
	{
		ThrowNativeError(SP_ERROR_PARAM, "Cannot set a definition validator after the definition is finalized.");
		return 0;
	}

	Function func = GetNativeFunction(2);
	definition.SetDefinitionValidator(plugin, func);
	return 0;
}


public int Native_FinalizeCreateDefinition(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	if (definition.Finalize() == false)
	{
		definition = null;
	}
	return view_as<int>(definition);
}



public int Native_GetDefinitionNameLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	return view_as<int>(definition.NameLength);
}

public int Native_GetDefinitionName(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int bufferSize = GetNativeCell(3);
	char[] buffer = new char[bufferSize];

	definition.GetName(buffer, bufferSize);
	SetNativeString(2, buffer, bufferSize);
	return true;
}

public int Native_GetDefinitionParent(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	return view_as<int>(definition.Parent);
}

public int Native_GetIsTemplate(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	return view_as<int>(definition.IsTemplate);
}

public int Native_GetIsComplete(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	return view_as<int>(definition.IsComplete);
}

public int Native_IsDefinitionFromPlugin(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	Handle otherPlugin = GetNativeCell(2);
	bool checkParents = GetNativeCell(3);
	return view_as<int>(definition.IsFromPlugin(otherPlugin, checkParents));
}

public int Native_GetDefinitionChildren(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));
	bool directDescendants = GetNativeCell(2);
	bool filterAbstracts = GetNativeCell(3);

	ArrayList children = definition.GetChildren(directDescendants, filterAbstracts);
	Handle clonedChildren = CloneHandle(children, plugin);
	delete children;

	return view_as<int>(clonedChildren);
}

public int Native_GetDefinitionInt(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int value;
	if (Definition_GetPropertyInt(definition, property, value) == false)
	{
		return false;
	}

	SetNativeCellRef(3, value);
	return true;
}

public int Native_GetDefinitionBool(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	bool value;
	if (Definition_GetPropertyBool(definition, property, value) == false)
	{
		return false;
	}

	SetNativeCellRef(3, value);
	return true;
}

public int Native_GetDefinitionCell(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	any value;
	if (Definition_GetPropertyCell(definition, property, value) == false)
	{
		return false;
	}

	SetNativeCellRef(3, value);
	return true;
}

public int Native_GetDefinitionFloat(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	float value;
	if (Definition_GetPropertyFloat(definition, property, value) == false)
	{
		return false;
	}

	SetNativeCellRef(3, value);
	return true;
}

public int Native_GetDefinitionString(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	int bufferSize = GetNativeCell(4);
	char[] buffer = new char[bufferSize];

	DString string;
	if (Definition_GetPropertyString(definition, property, string) == false)
	{
		return false;
	}
	
	string.Read(buffer, bufferSize);
	SetNativeString(3, buffer, bufferSize);
	return true;
}

public int Native_HasDefinitionFunction(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	return Definition_HasPropertyFunction(definition, property);
}

public int Native_StartDefinitionFunction(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	return Definition_StartPropertyFunction(definition, property);
}

public int Native_AddDefinitionPropertyToForward(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	Handle fwd = GetNativeCell(3);

	return Definition_AddPropertyToForward(definition, property, fwd);
}

public int Native_GetDefinitionDefinition(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	IDefinition value;
	if (Definition_GetPropertyDefinition(definition, property, value) == false)
	{
		return false;
	}

	SetNativeCellRef(3, value);
	return true;
}

public int Native_GetDefinitionArrayInt(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Int) == false)
	{
		return false;
	}

	int index = GetNativeCell(3);
	if (index < 0 || index >= array.Length)
	{
		return false;
	}

	SetNativeCellRef(4, array.Get(index));
	return true;
}

public int Native_GetDefinitionArrayBool(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Bool) == false)
	{
		return false;
	}

	int index = GetNativeCell(3);
	if (index < 0 || index >= array.Length)
	{
		return false;
	}

	SetNativeCellRef(4, array.Get(index));
	return true;
}

public int Native_GetDefinitionArrayCell(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Cell) == false)
	{
		return false;
	}

	int index = GetNativeCell(3);
	if (index < 0 || index >= array.Length)
	{
		return false;
	}

	SetNativeCellRef(4, array.Get(index));
	return true;
}

public int Native_GetDefinitionArrayFloat(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Float) == false)
	{
		return false;
	}

	int index = GetNativeCell(3);
	if (index < 0 || index >= array.Length)
	{
		return false;
	}

	SetNativeCellRef(4, array.Get(index));
	return true;
}

public int Native_GetDefinitionArrayString(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_String) == false)
	{
		return false;
	}

	int index = GetNativeCell(3);
	if (index < 0 || index >= array.Length)
	{
		return false;
	}

	int bufferSize = GetNativeCell(5);
	char[] buffer = new char[bufferSize];
	array.GetString(index, buffer, bufferSize);
	SetNativeString(4, buffer, bufferSize);
	return true;
}

public int Native_GetDefinitionArrayDefinition(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Definition) == false)
	{
		return false;
	}

	int index = GetNativeCell(3);
	if (index < 0 || index >= array.Length)
	{
		return false;
	}

	DReference reference = array.Get(index);
	SetNativeCellRef(4, reference.Definition);
	return true;
}

public int Native_GetDefinitionIntArrayLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Int) == false)
	{
		return false;
	}

	SetNativeCellRef(3, array.Length);
	return true;
}

public int Native_GetDefinitionBoolArrayLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Bool) == false)
	{
		return false;
	}

	SetNativeCellRef(3, array.Length);
	return true;
}

public int Native_GetDefinitionCellArrayLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Cell) == false)
	{
		return false;
	}

	SetNativeCellRef(3, array.Length);
	return true;
}

public int Native_GetDefinitionFloatArrayLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Float) == false)
	{
		return false;
	}

	SetNativeCellRef(3, array.Length);
	return true;
}

public int Native_GetDefinitionStringArrayLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);


	int arrayLength, stringMaxLength;
	if (Definition_GetPropertyStringArrayLength(definition, property, arrayLength, stringMaxLength))
	{
		SetNativeCellRef(3, arrayLength);
		SetNativeCellRef(4, stringMaxLength);
		return true;
	}
	return false;
}

public int Native_GetDefinitionDefinitionArrayLength(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Definition) == false)
	{
		return false;
	}

	SetNativeCellRef(3, array.Length);
	return true;
}

public int Native_GetDefinitionIntArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Int) == false)
	{
		return false;
	}

	int bufferSize = GetNativeCell(4);
	int[] buffer = new int[bufferSize];
	for (int i = Min(array.Length - 1, bufferSize); i >= 0; i--)
	{
		buffer[i] = array.Get(i);
	}
	SetNativeArray(3, buffer, bufferSize);
	return true;
}

public int Native_GetDefinitionBoolArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Bool) == false)
	{
		return false;
	}

	int bufferSize = GetNativeCell(4);
	bool[] buffer = new bool[bufferSize];
	for (int i = Min(array.Length - 1, bufferSize); i >= 0; i--)
	{
		buffer[i] = array.Get(i);
	}
	SetNativeArray(3, buffer, bufferSize);
	return true;
}

public int Native_GetDefinitionCellArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Cell) == false)
	{
		return false;
	}

	int bufferSize = GetNativeCell(4);
	any[] buffer = new any[bufferSize];
	for (int i = Min(array.Length - 1, bufferSize); i >= 0; i--)
	{
		buffer[i] = array.Get(i);
	}
	SetNativeArray(3, buffer, bufferSize);
	return true;
}

public int Native_GetDefinitionFloatArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Float) == false)
	{
		return false;
	}

	int bufferSize = GetNativeCell(4);
	float[] buffer = new float[bufferSize];
	for (int i = Min(array.Length - 1, bufferSize); i >= 0; i--)
	{
		buffer[i] = array.Get(i);
	}
	SetNativeArray(3, buffer, bufferSize);
	return true;
}

public int Native_GetDefinitionDefinitionArray(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	int length;
	GetNativeStringLength(2, length);
	
	length += 1;
	char[] property = new char[length];
	GetNativeString(2, property, length);

	ArrayList array;
	if (Definition_GetPropertyArray(definition, property, array, PropertyType_Definition) == false)
	{
		return false;
	}

	int bufferSize = GetNativeCell(4);
	Handle[] buffer = new Handle[bufferSize];
	for (int i = Min(array.Length - 1, bufferSize); i >= 0; i--)
	{
		DReference reference = array.Get(i);
		buffer[i] = reference.Definition;
	}
	SetNativeArray(3, buffer, bufferSize);
	return true;
}


public int Native_DefinitionDestroy(Handle plugin, int numParams)
{
	IDefinition definition = view_as<IDefinition>(GetNativeCell(1));

	if (plugin != definition.OwningPlugin)
	{
		ThrowNativeError(SP_ERROR_NATIVE, "Only the plugin that created the definition can destroy it.");
		return 0;
	}

	definition.Destroy();
	return true;
}