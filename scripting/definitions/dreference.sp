#if defined _DEFINITIONS_DREFERENCE_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_DREFERENCE_INCLUDED

static StringMap references;

void SetupReferences()
{
	references = new StringMap();
}

/**
 * Layout of DReference
 * - 1 : ReferencedBy
 * - 2 : Definition
 * - 3 : NameLength
 * - 4 : Name
 **/

methodmap DReference < DataPack
{
	public DReference(const char[] name)
	{
		IDefinition definition = FindDefinition(name);
		ArrayList referencedBy = new ArrayList();

		DReference reference = view_as<DReference>(new DataPack());
		reference.WriteCell(referencedBy);
		reference.WriteCell(definition);
		reference.WriteCell(strlen(name));
		reference.WriteString(name);

		return reference;
	}

	property ArrayList ReferencedBy
	{
		public get()
		{
			this.Reset();
			return view_as<ArrayList>(this.ReadCell());
		}
	}

	property int ReferenceCount
	{
		public get()
		{
			return this.ReferencedBy.Length;
		}
	}

	property Handle Definition
	{
		public get()
		{
			this.Reset();
			this.ReadCell(); // referenced by
			return this.ReadCell();
		}
	}

	property int NameLength
	{
		public get()
		{
			this.Reset();
			this.ReadCell(); // referenced by
			this.ReadCell(); // definition
			return this.ReadCell();
		}
	}

	property bool IsResolved
	{
		public get()
		{
			return this.Definition != null;
		}
	}

	public void ReadName(char[] buffer, int bufferSize)
	{
		this.Reset();
		this.ReadCell(); // reference count
		this.ReadCell(); // definition
		this.ReadCell(); // length
		this.ReadString(buffer, bufferSize);
	}

	public void AddReferenceBy(Handle definition)
	{
		ArrayList referencedBy = this.ReferencedBy;
		referencedBy.Push(definition);
	}

	public void RemoveReferenceBy(Handle definition)
	{
		ArrayList referencedBy = this.ReferencedBy;
		int index = referencedBy.FindValue(definition);
		if (index != -1)
		{
			referencedBy.Erase(index);
			if (referencedBy.Length == 0)
			{
				RemoveReference(this);
				delete this;
			}
		}
	}

	public void SetDefinition(Handle definition)
	{
		this.Reset();
		this.ReadCell(); // referenced by
		this.WriteCell(definition);
	}

	public void Resolve(Handle definition)
	{
		this.SetDefinition(definition);
		NotifyResolved(this.ReferencedBy);
	}

	public void Unresolve()
	{
		this.SetDefinition(null);
		NotifyUnresolved(this.ReferencedBy);
	}
}

static void RemoveReference(DReference reference)
{
	int length = reference.NameLength + 1;
	char[] name = new char[length];
	reference.ReadName(name, length);
	StringToLower(name, name, length);

	references.Remove(name);
}

static DReference FindReference(const char[] name)
{
	int length = strlen(name) + 1;
	char[] lowerCase = new char[length];
	StringToLower(name, lowerCase, length);

	DReference reference;
	if (references.GetValue(lowerCase, reference))
	{
		return reference;
	}
	return null;
}

static void NotifyResolved(ArrayList definitions)
{
	for (int i = definitions.Length - 1; i >= 0; i--)
	{
		IDefinition definition = view_as<IDefinition>(definitions.Get(i));
		Definition_NotifyResolved(definition);
	}
}

static void NotifyUnresolved(ArrayList definitions)
{
	for (int i = definitions.Length - 1; i >= 0; i--)
	{
		IDefinition definition = view_as<IDefinition>(definitions.Get(i));
		Definition_NotifyUnresolved(definition);
	}
}

DReference GetReference(const char[] name, Handle definition)
{
	DReference reference = FindReference(name);

	if (reference == null)
	{
		reference = new DReference(name);

		int length = strlen(name) + 1;
		char[] lowerCase = new char[length];
		StringToLower(name, lowerCase, length);
		references.SetValue(lowerCase, reference);
	}
	reference.AddReferenceBy(definition);

	return reference;
}

void ReferenceResolve(const char[] name, Handle definition)
{
	DReference reference = FindReference(name);
	if (reference != null)
	{
		reference.Resolve(definition);
	}
}

void ReferenceUnresolve(const char[] name)
{
	DReference reference = FindReference(name);
	if (reference != null)
	{
		reference.Unresolve();
	}
}