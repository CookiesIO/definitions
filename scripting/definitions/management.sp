#if defined _DEFINITIONS_MANAGEMENT_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_MANAGEMENT_INCLUDED

// Figured it'd make code cleaner and easier to maintain exposing access through
// functions and not giving direct access to these StringMaps and ArrayLists!

static StringMap g_hDefinitionMap;
static ArrayList g_hDefinitionList;

static StringMap g_hDefinitionCreatedListeners;
static StringMap g_hDefinitionDestroyedListeners;

static Handle g_hOnDefinitionDestroyedForward;
static Handle g_hOnDefinitionCreatedForward;

void SetupDefinitionManagement()
{
	g_hDefinitionMap = new StringMap();
	g_hDefinitionList = new ArrayList();

	g_hDefinitionCreatedListeners = new StringMap();
	g_hDefinitionDestroyedListeners = new StringMap();

	g_hOnDefinitionCreatedForward = CreateGlobalForward("OnDefinitionCreated", ET_Ignore, Param_Cell);
	g_hOnDefinitionDestroyedForward = CreateGlobalForward("OnDefinitionDestroyed", ET_Ignore, Param_Cell);
}

IDefinition FindDefinition(const char[] name, DefinitionState state=DefinitionState_Complete, bool exactState=true)
{
	int length = strlen(name) + 1;
	char[] lowerCase = new char[length];
	StringToLower(name, lowerCase, length);

	IDefinition definition;
	if (g_hDefinitionMap.GetValue(lowerCase, definition))
	{
		if ((exactState == true  && definition.State == state) ||
			(exactState == false && definition.State >= state))
		{
			return definition;
		}
	}
	return null;
}

/**
 * IDefinitionCreated occurs when the definition is /created/ regardless of whether
 * the definition is valid or not (missing references)
 **/
void IDefinitionCreated(IDefinition definition)
{
	int nameLength = definition.NameLength + 1;
	char[] name = new char[nameLength];
	definition.GetName(name, nameLength);
	StringToLower(name, name, nameLength);

	g_hDefinitionMap.SetValue(name, definition);
	g_hDefinitionList.Push(definition);
}

/**
 * IDefinitionDestroyed occurs when the definition is getting destroyed
 * at this point all it's children are still valid, we just want to remove
 * this from view prior to destroying children and sending notifications out
 **/
void IDefinitionDestroyed(IDefinition definition)
{
	int nameLength = definition.NameLength + 1;
	char[] name = new char[nameLength];
	definition.GetName(name, nameLength);
	StringToLower(name, name, nameLength);
	g_hDefinitionMap.Remove(name);

	int index = g_hDefinitionList.FindValue(definition);
	if (index != -1)
	{
		g_hDefinitionList.Erase(index);
	}
}

/**
 * NotifyDefinitionCreated occurs when the definition is finalized
 * The references are not neccesarily resolved, but it's good enough to create children from it
 **/
void NotifyDefinitionCreated(IDefinition definition)
{
	Call_StartForward(g_hOnDefinitionCreatedForward);
	Call_PushCell(definition);
	Call_Finish();
	
	int nameLength = definition.NameLength + 1;
	char[] name = new char[nameLength];
	definition.GetName(name, nameLength);
	StringToLower(name, name, nameLength);

	Handle fwd;
	if (g_hDefinitionCreatedListeners.GetValue(name, fwd))
	{
		Call_StartForward(fwd);
		Call_PushCell(definition);
		Call_Finish();
	}
}

/**
 * NotifyDefinitionDestroyed occurs when the definition is destroyed,
 * after all it's children are destroyed, so a parent is always fully destroyed
 * after it's children
 **/
void NotifyDefinitionDestroyed(IDefinition definition)
{
	int nameLength = definition.NameLength + 1;
	char[] name = new char[nameLength];
	definition.GetName(name, nameLength);

	ReferenceUnresolve(name);

	Call_StartForward(g_hOnDefinitionDestroyedForward);
	Call_PushCell(definition);
	Call_Finish();

	Handle fwd;
	if (g_hDefinitionDestroyedListeners.GetValue(name, fwd))
	{
		Call_StartForward(fwd);
		Call_PushCell(definition);
		Call_Finish();
	}
}

/**
 *
 * Listening
 *
 **/
void AddDefinitionCreatedListener(const char[] name, Handle plugin, Function callback)
{
	int length = strlen(name) + 1;
	char[] lowerCase = new char[length];
	StringToLower(name, lowerCase, length);

	// If the definition already exists and is complete, call the listener immediately
	// otherwise it might never be called, even though the definition exists
	IDefinition definition = FindDefinition(lowerCase);
	if (definition.IsValid(true) && definition.IsComplete)
	{
		Call_StartFunction(plugin, callback);
		Call_PushCell(definition);
		Call_Finish();
	}

	Handle fwd;
	if (g_hDefinitionCreatedListeners.GetValue(lowerCase, fwd) == false)
	{
		fwd = CreateForward(ET_Ignore, Param_Cell);
		g_hDefinitionCreatedListeners.SetValue(lowerCase, fwd);
	}
	AddToForward(fwd, plugin, callback);
}

void RemoveDefinitionCreatedListener(const char[] name, Handle plugin, Function callback)
{
	int length = strlen(name) + 1;
	char[] lowerCase = new char[length];
	StringToLower(name, lowerCase, length);

	Handle fwd;
	if (g_hDefinitionCreatedListeners.GetValue(lowerCase, fwd))
	{
		RemoveFromForward(fwd, plugin, callback);
	}
}

void AddDefinitionDestroyedListener(const char[] name, Handle plugin, Function callback)
{
	int length = strlen(name) + 1;
	char[] lowerCase = new char[length];
	StringToLower(name, lowerCase, length);

	Handle fwd;
	if (g_hDefinitionDestroyedListeners.GetValue(lowerCase, fwd) == false)
	{
		fwd = CreateForward(ET_Ignore, Param_Cell);
		g_hDefinitionDestroyedListeners.SetValue(lowerCase, fwd);
	}
	AddToForward(fwd, plugin, callback);
}

void RemoveDefinitionDestroyedListener(const char[] name, Handle plugin, Function callback)
{
	int length = strlen(name) + 1;
	char[] lowerCase = new char[length];
	StringToLower(name, lowerCase, length);

	Handle fwd;
	if (g_hDefinitionDestroyedListeners.GetValue(lowerCase, fwd))
	{
		RemoveFromForward(fwd, plugin, callback);
	}
}

/**
 *
 * House keeping
 *
 **/
void DestroyAllDefinitions()
{
	while (g_hDefinitionList.Length > 0)
	{
		IDefinition definition = view_as<IDefinition>(g_hDefinitionList.Get(0));
		definition.Destroy();
	}
}

void CleanupUnfinalizedDefinitions()
{
	// Shouldn't be a need to reset index, nothing should be able to depend on an unfinalized definition
	for (int i = g_hDefinitionList.Length - 1; i >= 0; i--)
	{
		IDefinition definition = view_as<IDefinition>(g_hDefinitionList.Get(0));
		if (definition.State == DefinitionState_Unfinalized)
		{
			int nameLength = definition.NameLength + 1;
			char[] name = new char[nameLength];
			definition.GetName(name, nameLength);
			LogError("Unfinalized definition cleaned up: %s", name);

			definition.Destroy();
		}
	}
}

void CleanupPluginDefinitions(Handle plugin)
{
	// Reverse loop to prevent bugs, also needs to reset i every time something is destroyed and the difference in length is > 1, *sigh*
	int length = g_hDefinitionList.Length;
	for (int i = length - 1; i >= 0; i--)
	{
		IDefinition definition = view_as<IDefinition>(g_hDefinitionList.Get(i));
		if (plugin == definition.OwningPlugin)
		{
			definition.Destroy();
			int newLength = g_hDefinitionList.Length;
			if ((length - newLength) > 1)
			{
				i = newLength;
			}
			length = newLength;
		}
	}
}

void PrintAllDefinitionsToClient(int client)
{
	int length = g_hDefinitionList.Length;
	for (int i = 0; i < length; i++)
	{
		IDefinition definition = view_as<IDefinition>(g_hDefinitionList.Get(i));

		int nameLength = definition.NameLength + 1;
		char[] name = new char[nameLength];
		definition.GetName(name, nameLength);

		IDefinition parent = definition.Parent;
		int parentNameLength = 1;
		if (parent != null)
		{
			parentNameLength = parent.NameLength + 1;
		}
		char[] parentName = new char[parentNameLength];
		if (parent != null)
		{
			parent.GetName(parentName, parentNameLength);
		}

		char state[32];
		switch (definition.State)
		{
			case DefinitionState_Unfinalized:
			{
				strcopy(state, sizeof(state), "Unfinalized");
			}
			case DefinitionState_Incomplete:
			{
				strcopy(state, sizeof(state), "Incomplete");
			}
			case DefinitionState_Complete:
			{
				strcopy(state, sizeof(state), "Complete");
			}
		}

		PrintToConsole(client, "%s (%s), %s", name, parentName, state);
	}
}