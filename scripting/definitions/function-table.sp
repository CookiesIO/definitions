#if defined _DEFINITIONS_FUNCTION_TABLE_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_FUNCTION_TABLE_INCLUDED

/**
 * Layout of FunctionTable
 * - 1 : Size
 * - 2 : FunctionCount
 * - 3 : Plugin
 * - 4 : Function
 * - *Repeat 3-4*
 **/

methodmap FunctionTable < DataPack
{
	property int IntPosition
	{
		public get()
		{
			return view_as<int>(this.Position);
		}
		public set(int value)
		{
			this.Position = view_as<DataPackPos>(value);
		}
	}

	public FunctionTable(FunctionTable original)
	{
		FunctionTable functionTable = view_as<FunctionTable>(new DataPack());

		if (original == null)
		{
			// Size includes itself, it marks the end of the DataPack
			// Doing it this way to make distance from implementation details and making assumptions of sizes
			functionTable.WriteCell(0); // size
			functionTable.WriteCell(0); // functionCount
			int size = functionTable.IntPosition;
			functionTable.Reset();
			functionTable.WriteCell(size);
		}
		else
		{
			// If we have a function table to copy, we just do so.
			original.Reset();
			int size = original.ReadCell();
			int functionCount = original.ReadCell();
			functionTable.WriteCell(size);
			functionTable.WriteCell(functionCount);
			for (int i = 0; i < functionCount; i++)
			{
				functionTable.WriteCell(original.ReadCell());
				functionTable.WriteFunction(original.ReadFunction());
			}
			// Rewrite size in again, this is due to a weird bug in the datapack
			// ._.
			// I should make a bug report about it...
			// Issue is, m_size in CDataPack is increased every time the datapack is written to
			// even if the data being written on, is not outside of the boundaries
			// in most normal cases this would not be noticeable, but in this case it is
			// Assignment of m_size in CDataPack should probably be moved to CheckSize (where it's increasing capacity)
			// and SetPosition should allow to set Pos to m_size
			functionTable.Reset();
			functionTable.WriteCell(size);
		}

		return functionTable;
	}

	public bool GetFunction(int offset, Handle &plugin, Function &func)
	{
		this.Reset();
		int size = this.ReadCell();
		if (size <= offset) {
			return false;
		}
		this.IntPosition = offset;
		plugin = this.ReadCell();
		func = this.ReadFunction();
		return true;
	}

	public bool SetFunction(int offset, Handle plugin, Function func)
	{
		this.Reset();
		int size = this.ReadCell();
		if (size <= offset) {
			return false;
		}
		this.IntPosition = offset;
		this.WriteCell(plugin);
		this.WriteFunction(func);
		return true;
	}

	public int AddFunction(Handle plugin, Function func)
	{
		this.Reset();
		int size = this.ReadCell();
		int functionCount = this.ReadCell();
		this.IntPosition = size;
		this.WriteCell(plugin);
		this.WriteFunction(func);
		int newSize = this.IntPosition;
		this.Reset();
		this.WriteCell(newSize);
		this.WriteCell(functionCount + 1);
		return size;
	}

	public bool StartCall(int offset)
	{
		Handle plugin;
		Function func;
		if (this.GetFunction(offset, plugin, func))
		{
			Call_StartFunction(plugin, func);
			return true;
		}
		return false;
	}

	public bool AddToForward(int offset, Handle fwd)
	{
		Handle plugin;
		Function func;
		if (this.GetFunction(offset, plugin, func))
		{
			AddToForward(fwd, plugin, func);
			return true;
		}
		return false;
	}
}