#if defined _DEFINITIONS_PROPERTIES_STRING_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_STRING_INCLUDED

bool Definition_DefinePropertyString(IDefinition definition, const char[] property, int &index=0)
{
	return definition.DefineProperty(property, PropertyType_String, index);
}

bool Definition_SetPropertyString(IDefinition definition, const char[] property, const char[] value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyString(definition, property, index);
		}

		DString string;
		if (definition.GetProperty(index, string, PropertyType_String) && string.Definition == definition)
		{
			string.Copy(value);
			return true;
		}

		string = new DString(definition, value);
		if (!definition.SetProperty(index, string, PropertyType_String))
		{
			delete string;
			return false;
		}
		return true;
	}
	return false;
}

bool Definition_GetPropertyString(IDefinition definition, const char[] property, DString &value)
{
	int index = definition.GetPropertyIndex(property);
	return definition.GetProperty(index, value, PropertyType_String);
}