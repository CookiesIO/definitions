#if defined _DEFINITIONS_PROPERTIES_REFERENCE_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_REFERENCE_INCLUDED

static int ReferenceRestriction_Parent = DefProp_Restriction1;

bool Definition_DefinePropertyDefinition(IDefinition definition, const char[] property, const char[] ancestor, int &index=0)
{
	if (definition.DefineProperty(property, PropertyType_Definition, index))
	{
		DReference parent = null;
		if (strlen(ancestor) != 0)
		{
			parent = GetReference(ancestor, definition);
		}
		definition.SetRestriction(index, ReferenceRestriction_Parent, parent);
		return true;
	}
	return false;
}

bool Definition_SetPropertyDefinition(IDefinition definition, const char[] property, const char[] value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyDefinition(definition, property, NULL_STRING, index);
		}

		if (definition.VerifyPropertyType(index, PropertyType_Definition))
		{
			DReference reference;
			if (definition.GetProperty(index, reference, PropertyType_Definition))
			{
				reference.RemoveReferenceBy(definition);
			}

			reference = GetReference(value, definition);
			return definition.SetProperty(index, reference, PropertyType_Definition);
		}
	}
	return false;
}

bool Definition_GetPropertyDefinition(IDefinition definition, const char[] property, IDefinition &value)
{
	DReference reference;
	int index = definition.GetPropertyIndex(property);
	if (definition.GetProperty(index, reference, PropertyType_Definition))
	{
		if (reference.IsResolved)
		{
			value = view_as<IDefinition>(reference.Definition);
			return true;
		}
	}
	return false;
}

DReference IDefinition_GetDefinitionParent(IDefinition definition, int index)
{
	return definition.GetRestriction(index, ReferenceRestriction_Parent);
}