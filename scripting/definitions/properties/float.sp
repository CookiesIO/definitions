#if defined _DEFINITIONS_PROPERTIES_FLOAT_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_FLOAT_INCLUDED

static int FloatRestriction_HasMin = DefProp_Restriction1;
static int FloatRestriction_Min = DefProp_Restriction2;
static int FloatRestriction_HasMax = DefProp_Restriction3;
static int FloatRestriction_Max = DefProp_Restriction4;

bool Definition_DefinePropertyFloat(IDefinition definition, const char[] property, int &index=0, bool hasMin=false, float min=0.0, bool hasMax=false, float max=0.0)
{
	if (definition.DefineProperty(property, PropertyType_Float, index))
	{
		if (hasMin && hasMax && min >= max)
		{
			LogError("Defining property '%s', Min(%f) is larger or equal to Max(%f)", property, min, max);
			return false;
		}
		definition.SetRestriction(index, FloatRestriction_HasMin, hasMin);
		definition.SetRestriction(index, FloatRestriction_Min, min);
		definition.SetRestriction(index, FloatRestriction_HasMax, hasMax);
		definition.SetRestriction(index, FloatRestriction_Max, max);
		return true;
	}
	return false;
}

bool Definition_SetPropertyFloat(IDefinition definition, const char[] property, float value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyFloat(definition, property, index);
		}
		if (index != -1)
		{
			if (definition.GetRestriction(index, FloatRestriction_HasMin))
			{
				float min = definition.GetRestriction(index, FloatRestriction_Min);
				if (value < min)
				{
					LogMessage("Setting float value on '%s' had a smaller value(%f) than min(%f)", property, value, min);
					value = min;
				}
			}
			else if (definition.GetRestriction(index, FloatRestriction_HasMax))
			{
				float max = definition.GetRestriction(index, FloatRestriction_Max);
				if (value > max)
				{
					LogMessage("Setting float value on '%s' had a larger value(%f) than max(%f)", property, value, max);
					value = max;
				}
			}
		}
		return definition.SetProperty(index, value, PropertyType_Float);
	}
	return false;
}

bool Definition_GetPropertyFloat(IDefinition definition, const char[] property, float &value)
{
	int index = definition.GetPropertyIndex(property);
	return definition.GetProperty(index, value, PropertyType_Float);
}