#if defined _DEFINITIONS_PROPERTIES_ARRAY_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_ARRAY_INCLUDED

static int ArrayRestriction_Type = DefProp_Restriction1;
static int ArrayRestriction_BlockSize = DefProp_Restriction2;
static int ArrayRestriction_StringMaxLength = DefProp_Restriction3;
static int ArrayRestriction_ParentReference = DefProp_Restriction4;

bool Definition_DefinePropertyArray(IDefinition definition, const char[] property, PropertyType arrayType, int &index=0)
{
	if (definition.DefineProperty(property, PropertyType_Array, index))
	{
		definition.SetRestriction(index, ArrayRestriction_Type, arrayType);
		definition.SetRestriction(index, ArrayRestriction_BlockSize, 1);
		return true;
	}
	return false;
}

// Strings need a special case
bool Definition_DefinePropertyStringArray(IDefinition definition, const char[] property, int maxLength)
{
	int index;
	if (definition.DefineProperty(property, PropertyType_Array, index))
	{
		definition.SetRestriction(index, ArrayRestriction_Type, PropertyType_String);
		definition.SetRestriction(index, ArrayRestriction_BlockSize, ByteCountToCells(maxLength));
		definition.SetRestriction(index, ArrayRestriction_StringMaxLength, maxLength);
		return true;
	}
	return false;
}

// We're going to use a specially special one for reference arrays, cause ancestors are awesome
bool Definition_DefinePropertyDefinitionArray(IDefinition definition, const char[] property, const char[] ancestor, int &index=0)
{
	if (definition.DefineProperty(property, PropertyType_Array, index))
	{
		definition.SetRestriction(index, ArrayRestriction_Type, PropertyType_Definition);
		definition.SetRestriction(index, ArrayRestriction_BlockSize, 1);
		DReference parent = null;
		if (strlen(ancestor) != 0)
		{
			parent = GetReference(ancestor, definition);
		}
		definition.SetRestriction(index, ArrayRestriction_ParentReference, parent);
		return true;
	}
	return false;
}

bool Definition_ClearPropertyArray(IDefinition definition, const char[] property, PropertyType arrayType)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			switch (arrayType)
			{
				case PropertyType_Int,
					 PropertyType_Bool,
					 PropertyType_Cell,
					 PropertyType_Float:
				{
					Definition_DefinePropertyArray(definition, property, arrayType, index);
				}
				case PropertyType_Definition:
				{
					Definition_DefinePropertyDefinitionArray(definition, property, NULL_STRING, index);
				}
				case PropertyType_String:
				{
					// We can't support strings like this unfortunately
					// string arrays /must/ be defined first, to set max length
				}
				default:
				{
					LogError("Should not happen: Unknown property type (%d) for property '%s'.", arrayType, property);
				}
			}
		}
		if (index != -1 &&
			definition.GetPropertyType(index) == PropertyType_Array &&
			definition.GetRestriction(index, ArrayRestriction_Type) == arrayType)
		{
			ArrayList array;
			if (definition.GetProperty(index, array, PropertyType_Array))
			{
				// If the array type is reference, we need to remove ourselves from the references
				if (arrayType == PropertyType_Definition)
				{
					RemoveReferenceByOnAll(definition, array);
				}
				array.Clear();
			}
			// As long as the type is Array a call to clear returns true, even if we haven't created the array yet
			return true;
		}
	}
	return false;
}

// We only create the array when pushing items to it
bool Definition_PushToPropertyArray(IDefinition definition, const char[] property, any value, PropertyType arrayType)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyArray(definition, property, arrayType, index);
		}
		if (definition.GetPropertyType(index) == PropertyType_Array &&
			definition.GetRestriction(index, ArrayRestriction_Type) == arrayType)
		{
			ArrayList array;
			if (!definition.GetProperty(index, array, PropertyType_Array))
			{
				array = new ArrayList();
				definition.SetProperty(index, array, PropertyType_Array);
			}
			array.Push(value);
			return true;
		}
	}
	return false;
}

// Need a special case for Strings ...
bool Definition_PushStringToPropertyArray(IDefinition definition, const char[] property, const char[] value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index != -1 &&
			definition.GetPropertyType(index) == PropertyType_Array &&
			definition.GetRestriction(index, ArrayRestriction_Type) == PropertyType_String)
		{
			ArrayList array;
			if (!definition.GetProperty(index, array, PropertyType_Array))
			{
				int blockSize = definition.GetRestriction(index, ArrayRestriction_BlockSize);
				array = new ArrayList(blockSize);
				definition.SetProperty(index, array, PropertyType_Array);
			}
			array.PushString(value);
			return true;
		}
	}
	return false;
}

// ... and also References
bool Definition_PushDefinitionToPropertyArray(IDefinition definition, const char[] property, const char[] value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyDefinitionArray(definition, property, "", index);
		}
		if (definition.GetPropertyType(index) == PropertyType_Array &&
			definition.GetRestriction(index, ArrayRestriction_Type) == PropertyType_Definition)
		{
			ArrayList array;
			if (!definition.GetProperty(index, array, PropertyType_Array))
			{
				int blockSize = definition.GetRestriction(index, ArrayRestriction_BlockSize);
				array = new ArrayList(blockSize);
				definition.SetProperty(index, array, PropertyType_Array);
			}

			DReference reference = GetReference(value, definition);
			array.Push(reference);
			return true;
		}
	}
	return false;
}

bool Definition_GetPropertyArray(IDefinition definition, const char[] property, ArrayList &value, PropertyType arrayType)
{
	int index = definition.GetPropertyIndex(property);
	if (definition.GetPropertyType(index) == PropertyType_Array && definition.GetRestriction(index, ArrayRestriction_Type) == arrayType)
	{
		if (!definition.GetProperty(index, value, PropertyType_Array))
		{
			// No array is just considered an empty one, so we want to return an empty array rather than null
			static ArrayList emptyArray = null;
			if (emptyArray == null) {
				emptyArray = new ArrayList();
			}
			value = emptyArray;
		}
		return true;
	}
	return false;
}

bool Definition_GetPropertyStringArrayLength(IDefinition definition, const char[] property, int &arrayLength, int &stringMaxLength)
{
	int index = definition.GetPropertyIndex(property);
	if (definition.GetPropertyType(index) == PropertyType_Array && definition.GetRestriction(index, ArrayRestriction_Type) == PropertyType_String)
	{
		ArrayList array;
		if (definition.GetProperty(index, array, PropertyType_Array))
		{
			arrayLength = array.Length;
		}
		else
		{
			arrayLength = 0;
		}
		stringMaxLength = definition.GetRestriction(index, ArrayRestriction_BlockSize);
		return true;
	}
	return false;
}

PropertyType IDefinition_GetArrayType(IDefinition definition, int index)
{
	return definition.GetRestriction(index, ArrayRestriction_Type);
}

DReference IDefinition_GetDefinitionArrayParent(IDefinition definition, int index)
{
	return definition.GetRestriction(index, ArrayRestriction_ParentReference);
}

static void RemoveReferenceByOnAll(IDefinition definition, ArrayList array)
{
	for (int i = array.Length - 1; i >= 0; i--)
	{
		DReference reference = array.Get(i);
		reference.RemoveReferenceBy(definition);
	}
}