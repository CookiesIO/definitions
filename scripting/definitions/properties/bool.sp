#if defined _DEFINITIONS_PROPERTIES_BOOL_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_BOOL_INCLUDED

bool Definition_DefinePropertyBool(IDefinition definition, const char[] property, int &index=0)
{
	return definition.DefineProperty(property, PropertyType_Bool, index);
}

bool Definition_SetPropertyBool(IDefinition definition, const char[] property, bool value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyBool(definition, property, index);
		}
		return definition.SetProperty(index, value, PropertyType_Bool);
	}
	return false;
}

bool Definition_GetPropertyBool(IDefinition definition, const char[] property, bool &value)
{
	int index = definition.GetPropertyIndex(property);
	return definition.GetProperty(index, value, PropertyType_Bool);
}