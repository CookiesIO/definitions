#if defined _DEFINITIONS_PROPERTIES_INT_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_INT_INCLUDED

static int IntRestriction_HasMin = DefProp_Restriction1;
static int IntRestriction_Min = DefProp_Restriction2;
static int IntRestriction_HasMax = DefProp_Restriction3;
static int IntRestriction_Max = DefProp_Restriction4;

bool Definition_DefinePropertyInt(IDefinition definition, const char[] property, int &index=0, bool hasMin=false, int min=0, bool hasMax=false, int max=0)
{
	if (definition.DefineProperty(property, PropertyType_Int, index))
	{
		if (hasMin && hasMax && min >= max)
		{
			LogError("Defining property '%s', Min(%d) is larger or equal to Max(%d)", property, min, max);
			return false;
		}
		definition.SetRestriction(index, IntRestriction_HasMin, hasMin);
		definition.SetRestriction(index, IntRestriction_Min, min);
		definition.SetRestriction(index, IntRestriction_HasMax, hasMax);
		definition.SetRestriction(index, IntRestriction_Max, max);
		return true;
	}
	return false;
}

bool Definition_SetPropertyInt(IDefinition definition, const char[] property, int value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyInt(definition, property, index);
		}
		if (index != -1)
		{
			if (definition.GetRestriction(index, IntRestriction_HasMin))
			{
				int min = definition.GetRestriction(index, IntRestriction_Min);
				if (value < min)
				{
					LogMessage("Setting int value on '%s' had a smaller value(%d) than min(%d)", property, value, min);
					value = min;
				}
			}
			else if (definition.GetRestriction(index, IntRestriction_HasMax))
			{
				int max = definition.GetRestriction(index, IntRestriction_Max);
				if (value > max)
				{
					LogMessage("Setting int value on '%s' had a larger value(%d) than max(%d)", property, value, max);
					value = max;
				}
			}
		}
		return definition.SetProperty(index, value, PropertyType_Int);
	}
	return false;
}

bool Definition_GetPropertyInt(IDefinition definition, const char[] property, int &value)
{
	int index = definition.GetPropertyIndex(property);
	return definition.GetProperty(index, value, PropertyType_Int);
}