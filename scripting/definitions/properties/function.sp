#if defined _DEFINITIONS_PROPERTIES_FUNCTION_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_FUNCTION_INCLUDED

bool Definition_DefinePropertyFunction(IDefinition definition, const char[] property, int &index=0)
{
	return definition.DefineProperty(property, PropertyType_Function, index);
}

bool Definition_SetPropertyFunction(IDefinition definition, const char[] property, Handle plugin, Function func)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyFunction(definition, property, index);
		}

		// We don't want to screw up our function table
		if (definition.GetPropertyType(index) == PropertyType_Function)
		{
			// First see if we have the function already, in which case we need to override it
			int offset;
			if (Definition_GetPropertyFunctionOffset(definition, property, offset))
			{
				definition.FunctionTable.SetFunction(offset, plugin, func);
				return true;
			}

			// If not, well, then we'll have to add a new entry to the function table
			offset = definition.FunctionTable.AddFunction(plugin, func);
			if (definition.SetProperty(index, offset, PropertyType_Function) == false)
			{
				// This shouldn't happen, but we say it in case it does
				LogError("Definitions: Somehow failed to set a property function when it should've been possible, property: '%s'", property);
				return false;
			}
			return true;
		}
	}
	return false;
}

bool Definition_GetPropertyFunctionOffset(IDefinition definition, const char[] property, int &value)
{
	int index = definition.GetPropertyIndex(property);
	return definition.GetProperty(index, value, PropertyType_Function);
}

bool Definition_HasPropertyFunction(IDefinition definition, const char[] property)
{
	int offset;
	if (Definition_GetPropertyFunctionOffset(definition, property, offset))
	{
		return true;
	}
	return false;
}

bool Definition_StartPropertyFunction(IDefinition definition, const char[] property)
{
	int offset;
	if (Definition_GetPropertyFunctionOffset(definition, property, offset))
	{
		definition.FunctionTable.StartCall(offset);
		return true;
	}
	return false;
}

bool Definition_AddPropertyToForward(IDefinition definition, const char[] property, Handle fwd)
{
	int offset;
	if (Definition_GetPropertyFunctionOffset(definition, property, offset))
	{
		definition.FunctionTable.AddToForward(offset, fwd);
		return true;
	}
	return false;
}