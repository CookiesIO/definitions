#if defined _DEFINITIONS_PROPERTIES_CELL_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_PROPERTIES_CELL_INCLUDED

bool Definition_DefinePropertyCell(IDefinition definition, const char[] property, int &index=0)
{
	return definition.DefineProperty(property, PropertyType_Cell, index);
}

bool Definition_SetPropertyCell(IDefinition definition, const char[] property, any value)
{
	if (definition.CanSetProperties())
	{
		int index = definition.GetPropertyIndex(property);
		if (index == -1)
		{
			Definition_DefinePropertyCell(definition, property, index);
		}
		return definition.SetProperty(index, value, PropertyType_Cell);
	}
	return false;
}

bool Definition_GetPropertyCell(IDefinition definition, const char[] property, any &value)
{
	int index = definition.GetPropertyIndex(property);
	return definition.GetProperty(index, value, PropertyType_Cell);
}