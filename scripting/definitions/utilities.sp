void StringToLower(const char[] input, char[] output, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (IsCharUpper(input[i]))
		{
			output[i] = CharToLower(input[i]);
		}
		else
		{
			output[i] = input[i];
		}
	}
}

int Min(int val1, int val2)
{
	return val1 < val2 ? val1 : val2;
}