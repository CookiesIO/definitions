#if defined _DEFINITIONS_DSTRING_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_DSTRING_INCLUDED

/**
 * Layout of DString (Shortened from DefinitionString)
 * - 1 : Definition
 * - 2 : Length
 * - 3 : String
 **/

methodmap DString < DataPack
{
	public DString(Handle definition, const char[] str)
	{
		DString handle = view_as<DString>(new DataPack());
		handle.WriteCell(definition);
		handle.WriteCell(strlen(str));
		handle.WriteString(str);
		return handle;
	}

	property Handle Definition
	{
		public get()
		{
			this.Reset();
			return this.ReadCell();
		}
	}

	property int Length
	{
		public get()
		{
			this.Reset();
			this.ReadCell();
			return this.ReadCell();
		}
	}

	public void Copy(const char[] str)
	{
		Handle definition = this.Definition;

		this.Reset(true);
		this.WriteCell(definition);
		this.WriteCell(strlen(str));
		this.WriteString(str);
	}

	public void Read(char[] buffer, int bufferSize)
	{
		this.Reset();
		this.ReadCell();
		this.ReadCell();
		this.ReadString(buffer, bufferSize);
	}
}