#if defined _DEFINITIONS_INTERNALDEFITION_INCLUDED
  #endinput
#endif
#define _DEFINITIONS_INTERNALDEFITION_INCLUDED

enum PropertyType
{
	PropertyType_Invalid,
	PropertyType_None,
	PropertyType_Int,		// restriction 1: min?, restriction 2: min, restriction 3: max?, restriction 4: max
	PropertyType_Bool,		// no restrictions
	PropertyType_Cell,		// no restrictions
	PropertyType_Float,		// restriction 1: min?, restriction 2: min, restriction 3: max?, restriction 4: max
	PropertyType_String,	// restriction 1: max length (def; limited by memory)
	PropertyType_Function,	// no restrictions
	PropertyType_Definition,// restriction 1: parent reference
	PropertyType_Array		// restriction 1: property type, restriction 2: max length(for strings, required), restriction 3: parent reference (for references, optional)
};

enum DefinitionState
{
	DefinitionState_Unfinalized,	// Still being constructed, not finalized by the owning plugin yet
	DefinitionState_Incomplete,		// Finalized but incomplete, missing some/all definition references
	DefinitionState_Complete,		// Finalized and complete, ready to use and abuse
};

/**
 * Layout of definitions (internally ArrayLists):
 * - OwningPlugin : Plugin Handle
 * - Name : DString
 * - Parent : ArrayList (definition)
 * - Children : ArrayList
 * - State : DefinitionState
 * - IsTemplate : Boolean
 * - FunctionTable : FunctionTable
 * - PropertyMap : StringMap
 * Properties of a definition:
 * - Immutable
 * - Can be templates, which is when a definition is defining a field, but not setting it,
 *   all children will be templates until they set all properties
 */

// Definition "structure"
enum
{
	Def_OwningPlugin = 0,
	Def_Name,
	Def_Parent,
	Def_Children,
	Def_State,
	Def_IsTemplate,
	Def_FunctionTable,
	Def_PropertyMap,
	Def_DefinitionValidator,
	Def_PropertiesStart
}

// What the definition array contains in each block
enum
{
	DefProp_Value = 0,
	DefProp_IsSet,
	DefProp_Type,
	DefProp_Restriction1,
	DefProp_Restriction2,
	DefProp_Restriction3,
	DefProp_Restriction4,
	DefProp_BLOCKSIZE
};

// We (almost) just want the tag now :|
// I had it fully as a methodmap, but it was a pain working with it.
// I've exposed the basics as method map properties/functions, but made the different
// property types defining/accessing/setting stuff functional rather than methodmappy.
// Internally only though, externally it'll still remain like a methodmap.
methodmap IDefinition < ArrayList
{
	property Handle OwningPlugin
	{
		public get()
		{
			return this.Get(Def_OwningPlugin);
		}
	}

	property DString Name
	{
		public get()
		{
			return this.Get(Def_Name);
		}
	}

	property int NameLength
	{
		public get()
		{
			DString name = this.Name;
			return name.Length;
		}
	}

	property IDefinition Parent
	{
		public get()
		{
			return this.Get(Def_Parent);
		}
	}

	property ArrayList Children
	{
		public get()
		{
			return this.Get(Def_Children);
		}
	}

	property bool IsTemplate
	{
		public get()
		{
			return this.Get(Def_IsTemplate);
		}
	}

	property DefinitionState State
	{
		public get()
		{
			return this.Get(Def_State);
		}
		public set(DefinitionState state)
		{
			return this.Set(Def_State, state);
		}
	}

	property bool IsFinalized
	{
		public get()
		{
			return this.State != DefinitionState_Unfinalized;
		}
	}

	property bool IsIncomplete
	{
		public get()
		{
			DefinitionState state = this.State;
			return state == DefinitionState_Incomplete;
		}
	}

	property bool IsComplete
	{
		public get()
		{
			return this.State == DefinitionState_Complete;
		}
	}

	property FunctionTable FunctionTable
	{
		public get()
		{
			return this.Get(Def_FunctionTable);
		}
	}

	property StringMap PropertyMap
	{
		public get()
		{
			return this.Get(Def_PropertyMap);
		}
	}

	property int DefinitionValidatorOffset
	{
		public get()
		{
			return this.Get(Def_DefinitionValidator);
		}
	}

	public void GetName(char[] buffer, int bufferSize)
	{
		DString name = this.Name;
		name.Read(buffer, bufferSize);
	}

	public StringMapSnapshot SnapshotProperties()
	{
		return this.PropertyMap.Snapshot();
	}

	public bool IsFromPlugin(Handle plugin, bool checkParents)
	{
		return IsFromPlugin(this, plugin, checkParents);
	}

	public ArrayList GetChildren(bool directDescendants, bool filterTemplates)
	{
		ArrayList result = new ArrayList();
		AddChildrenToArray(result, this, directDescendants, filterTemplates);
		return result;
	}

	public bool IsValid(bool allowTemplate)
	{
		return this != null && (allowTemplate || this.IsTemplate == false);
	}

	public bool IsDescendantOf(IDefinition other, bool direct=false)
	{
		if (other == null)
		{
			return false;
		}

		if (direct)
		{
			return this.Parent == other;
		}
		else
		{
			IDefinition parent = this;
			while ((parent = parent.Parent) != null)
			{
				if (parent == other)
				{
					return true;
				}
			}
		}
		return false;
	}

	public int GetPropertyIndex(const char[] name)
	{
		int index;
		if (this.PropertyMap.GetValue(name, index))
		{
			return index;
		}
		return -1;
	}

	public any GetRestriction(int index, int restriction)
	{
		if (restriction < DefProp_Restriction1 || restriction > DefProp_Restriction4)
		{
			ThrowError("Invalid restriction accessed: %d!", restriction);
			return 0;
		}
		return this.Get(index, restriction);
	}

	public void SetRestriction(int index, int restriction, any value)
	{
		if (restriction < DefProp_Restriction1 || restriction > DefProp_Restriction4)
		{
			ThrowError("Invalid restriction accessed: %d!", restriction);
			return;
		}
		this.Set(index, value, restriction);
	}

	public PropertyType GetPropertyType(int index)
	{
		if (index == -1 || index >= this.Length)
		{
			return PropertyType_None;
		}
		if (index < Def_PropertiesStart)
		{
			return PropertyType_Invalid;
		}
		return this.Get(index, DefProp_Type);
	}

	public bool VerifyPropertyType(int index, PropertyType type)
	{
		PropertyType propertyType = this.GetPropertyType(index);
		return propertyType == type;
	}

	public bool CanGetProperty(int index, PropertyType type)
	{
		bool result = this.VerifyPropertyType(index, type);
		if (result)
		{
			result = this.Get(index, DefProp_IsSet);
		}
		return result;
	}

	public bool GetProperty(int index, any &value, PropertyType type)
	{
		if (this.CanGetProperty(index, type))
		{
			value = this.Get(index, DefProp_Value);
			return true;
		}
		return false;
	}

	public bool CanGetPropertyUntyped(int index)
	{
		bool result = this.GetPropertyType(index) > PropertyType_None;
		if (result)
		{
			result = this.Get(index, DefProp_IsSet);
		}
		return result;
	}

	public bool GetPropertyUntypedEx(int index, any &value)
	{
		if (this.CanGetPropertyUntyped(index))
		{
			value = this.Get(index, DefProp_Value);
			return true;
		}
		return false;
	}

	public bool CanDefineProperty(const char[] property)
	{
		if (this.IsFinalized == false)
		{
			int index = this.GetPropertyIndex(property);
			return index == -1;
		}
		return false;
	}

	public bool DefineProperty(const char[] property, PropertyType type, int &index)
	{
		if (this.CanDefineProperty(property))
		{
			index = this.Push(0);
			this.Set(index, type, DefProp_Type);
			this.Set(index, false, DefProp_IsSet);
			this.PropertyMap.SetValue(property, index);
			return true;
		}
		return false;
	}

	public bool CanSetProperties()
	{
		return !this.IsFinalized;
	}

	public bool SetProperty(int index, any value, PropertyType type)
	{
		if (this.VerifyPropertyType(index, type))
		{
			this.Set(index, value, DefProp_Value);
			this.Set(index, true, DefProp_IsSet);
			return true;
		}
		return false;
	}

	public void SetDefinitionValidator(Handle plugin, Function func)
	{
		if (this.IsFinalized == false)
		{
			this.FunctionTable.SetFunction(this.DefinitionValidatorOffset, plugin, func);
		}
	}

	public void ForceTemplate()
	{
		if (this.IsFinalized == false)
		{
			this.Set(Def_IsTemplate, true);
		}
	}


	public void AddChild(IDefinition child)
	{
		this.Children.Push(child);
	}

	public void RemoveChild(IDefinition child)
	{
		ArrayList children = this.Children;
		int index = children.FindValue(child);
		if (index != -1)
		{
			children.Erase(index);
		}
	}

	public bool ExpectationsMetFor(IDefinition child)
	{
		bool result = true;
		IDefinition parent = this;
		while (result && parent != null)
		{
			int offset = parent.DefinitionValidatorOffset;
			parent.FunctionTable.StartCall(offset);
			Call_PushCell(child);
			Call_Finish(result);
			parent = parent.Parent;
		}
		return result;
	}

	public void CheckReferences()
	{
		if (this.IsComplete)
		{
			int nameLength = this.NameLength + 1;
			char[] name = new char[nameLength];
			this.GetName(name, nameLength);
			LogError("Definitions: CheckReferences on \"%s\" after completion, something must be wrong.", name);
			return;
		}

		IDefinition parent = this.Parent;
		if (parent != null && parent.IsIncomplete)
		{
			// ignore references if our parent is not even complete yet
			return;
		}

		for (int i = this.Length - 1; i >= Def_PropertiesStart; i--)
		{
			PropertyType type = this.GetPropertyType(i);
			switch (type)
			{
				case PropertyType_Definition:
				{
					DReference reference;
					DReference parentReference = IDefinition_GetDefinitionParent(this, i);
					if (parentReference != null && !parentReference.IsResolved)
					{
						return;
					}
					if (this.GetPropertyUntypedEx(i, reference))
					{
						// We can be pretty damn sure it's DefinitionState_Incomplete here
						if (!reference.IsResolved)
						{
							return;
						}

						// Verify parent
						IDefinition definition = view_as<IDefinition>(reference.Definition);
						if (parentReference != null && !definition.IsDescendantOf(view_as<IDefinition>(parentReference.Definition)))
						{
							int nameLength = this.NameLength + 1;
							char[] name = new char[nameLength];
							this.GetName(name, nameLength);

							DestroyDefinition(this);
							LogError("CheckReferences on \"%s\" found a reference that is not a descendant of the parent restriction, destoryed definition.", name);
						}
					}
				}
				case PropertyType_Array:
				{
					// This is a bit more tricky, takes a bit more time
					// If our array is an array of references, we have to loop through and make sure each reference is resolved
					ArrayList array;
					if (IDefinition_GetArrayType(this, i) == PropertyType_Definition && this.GetPropertyUntypedEx(i, array))
					{
						DReference parentReference = IDefinition_GetDefinitionArrayParent(this, i);
						if (parentReference != null && !parentReference.IsResolved)
						{
							return;
						}

						for (int j = array.Length - 1; j >= 0; j--)
						{
							DReference reference = array.Get(j);
							if (!reference.IsResolved)
							{
								return;
							}
						
							// Verify parent
							IDefinition definition = view_as<IDefinition>(reference.Definition);
							if (parentReference != null && !definition.IsDescendantOf(view_as<IDefinition>(parentReference.Definition)))
							{
								int nameLength = this.NameLength + 1;
								char[] name = new char[nameLength];
								this.GetName(name, nameLength);

								DestroyDefinition(this);
								LogError("CheckReferences on \"%s\" found a reference in an array that is not a descendant of the parent restriction, destroyed definition.", name);
							}
						}
					}
				}
			}
		}

		// Juuust to be sure, check with our parents if we are good enough
		// Good parents would tell you to be who you are
		// Others will do not-so-nice things, if you don't meet their expectations
		if (parent != null)
		{
			if (parent.ExpectationsMetFor(this) == false)
			{
				// Sawree
				int nameLength = this.NameLength + 1;
				char[] name = new char[nameLength];
				this.GetName(name, nameLength);

				DestroyDefinition(this);
				LogError("Parent expectations for \"%s\" were not met, destroyed definition.", name);
				return;
			}
		}

		// All references be good, so now we can PARTY!
		this.State = DefinitionState_Complete;

		int nameLength = this.NameLength + 1;
		char[] name = new char[nameLength];
		this.GetName(name, nameLength);
		ReferenceResolve(name, this);

		// We should also make our children check their references, could be they're complete
		ArrayList children = this.Children;
		for (int i = children.Length - 1; i >= 0; i--)
		{
			IDefinition definition = view_as<IDefinition>(children.Get(i));
			CheckReferences(definition);
		}
	}

	public void TurnIncomplete()
	{
		if (this.IsComplete)
		{
			this.State = DefinitionState_Incomplete;

			int nameLength = this.NameLength + 1;
			char[] name = new char[nameLength];
			this.GetName(name, nameLength);
			ReferenceUnresolve(name);

			// When turning incomplete, all children must also turn incomplete
			ArrayList children = this.Children;
			for (int i = children.Length - 1; i >= 0; i--)
			{
				IDefinition definition = view_as<IDefinition>(children.Get(i));
				TurnIncomplete(definition);
			}
		}
	}

	public void ReferenceResolved()
	{
		// Unfortunate workaround dealing with referencing the same definition twice
		if (this.IsComplete == false)
		{
			this.CheckReferences();
		}
	}

	public void ReferenceUnresolved()
	{
		this.TurnIncomplete();
	}

	public bool Finalize()
	{
		if (this.IsFinalized == false)
		{
			// Scan to see if this is template definition
			if (this.IsTemplate == false)
			{
				int length = this.Length;
				for (int i = Def_PropertiesStart; i < length; i++)
				{
					// Unset arrays are just empty, so they're allowed
					if (this.CanGetPropertyUntyped(i) == false && this.GetPropertyType(i) != PropertyType_Array)
					{
						this.ForceTemplate();
						i = length;
						//break; // Heap leak detected q_q
					}
				}
			}

			// Aw'right! Finalized! Now we're incomplete, and almost ready for action!
			this.State = DefinitionState_Incomplete;

			IDefinition parent = this.Parent;
			if (parent != null)
			{
				parent.AddChild(this);
			}

			// So we should check references, we might be complete
			this.CheckReferences();

			// No matter the outcome though, we'll notify we're created!
			NotifyDefinitionCreated(this);
			return true;
		}
		return false;
	}

	public void Destroy()
	{
		// Remove us from the global list, so we can't be discovered anymore
		IDefinitionDestroyed(this);

		// If we're finalized, we should remove ourself from everything
		if (this.IsFinalized)
		{
			IDefinition parent = this.Parent;
			if (parent != null)
			{
				parent.RemoveChild(this);
			}

			// Now that we're detached from the world, make sure to destroy our children first
			// It's a sad world definitions live in, needing to kill the children when the parents go away q_q
			ArrayList children = this.Children;
			for (int i = children.Length - 1; i >= 0; i--)
			{
				IDefinition definition = view_as<IDefinition>(children.Get(i));
				DestroyDefinition(definition);
			}
		}

		// Then we tell the owning plugin that we're destroying this definition
		// - not enclosed in IsFinalized, since it might contain data that should be freed!
		Handle owningPlugin = this.OwningPlugin;
		Function onDestroy = GetFunctionByName(owningPlugin, "OnDefinitionDestroy");
		if (onDestroy != INVALID_FUNCTION)
		{
			Call_StartFunction(owningPlugin, onDestroy);
			Call_PushCell(this);
			Call_Finish();
		}

		// We can notify the destruction to other plugins, as long as we were finalized
		if (this.IsFinalized)
		{
			NotifyDefinitionDestroyed(this);
		}

		// We also mustn't forget to close our strings and other possible handles.
		// We're doing this now in case they are neccesary in the above function and forward
		int length = this.Length;
		for (int i = Def_PropertiesStart; i < length; i++)
		{
			PropertyType type = this.GetPropertyType(i);
			switch (type)
			{
				case PropertyType_String:
				{
					DString value;
					if (this.GetPropertyUntypedEx(i, value))
					{
						if (value.Definition == this)
						{
							delete value;
						}
					}
				}
				case PropertyType_Definition:
				{
					DReference value;
					if (this.GetPropertyUntypedEx(i, value))
					{
						// We can do this even if we haven't explicitly referenced
						// this reference such as if it's inherited from a parent
						value.RemoveReferenceBy(this);
					}
				}
				case PropertyType_Array:
				{
					ArrayList array;
					if (this.GetPropertyUntypedEx(i, array))
					{
						if (IDefinition_GetArrayType(this, i) == PropertyType_Definition)
						{
							for (int j = array.Length - 1; j >= 0; j--)
							{
								DReference reference = array.Get(j);
								reference.RemoveReferenceBy(this);
							}
						}
						delete array;
					}
				}
			}
		}

		// Finally destroy our selves *sniff*
		delete this.Name;
		delete this.Children;
		delete this.PropertyMap;
		delete this.FunctionTable;
		delete this;
	}

	public IDefinition(Handle plugin, const char[] name, IDefinition parent)
	{
		IDefinition definition;
		StringMap properties = new StringMap();
		ArrayList children = new ArrayList();
		FunctionTable functionTable;
		int definitionValidatorOffset;

		if (parent == null)
		{
			definition = view_as<IDefinition>(new ArrayList(DefProp_BLOCKSIZE, Def_PropertiesStart));
			functionTable = new FunctionTable(null);
			definitionValidatorOffset = functionTable.AddFunction(null, DefaultDefinitionValidator);
		}
		else
		{
			// properties are stored in the definition array
			// the properties string map only stores references, those need to be copied
			definition = view_as<IDefinition>(parent.Clone());

			StringMapSnapshot snapshot = parent.SnapshotProperties();
			int length = snapshot.Length;
			for (int i = 0; i < length; i++)
			{
				int bufferSize = snapshot.KeyBufferSize(i);
				char[] key = new char[bufferSize];
				snapshot.GetKey(i, key, bufferSize);

				int index = parent.GetPropertyIndex(key);
				properties.SetValue(key, index);
			}
			delete snapshot;

			functionTable = new FunctionTable(parent.FunctionTable);
			definitionValidatorOffset = parent.DefinitionValidatorOffset;

			// Make sure to reset, no need to possibly call custom validation twice
			functionTable.SetFunction(definitionValidatorOffset, null, DefaultDefinitionValidator);

			// we need to clone the arrays created on the parent as well
			for (int i = definition.Length - 1; i >= Def_PropertiesStart; i--)
			{
				if (definition.GetPropertyType(i) == PropertyType_Array)
				{
					ArrayList array;
					if (definition.GetPropertyUntypedEx(i, array))
					{
						definition.Set(i, array.Clone(), DefProp_Value);
					}
				}
			}
		}

		definition.Set(Def_OwningPlugin, plugin);
		definition.Set(Def_Name, new DString(definition, name));
		definition.Set(Def_Parent, parent);
		definition.Set(Def_Children, children);
		definition.Set(Def_State, DefinitionState_Unfinalized);
		definition.Set(Def_IsTemplate, false);
		definition.Set(Def_FunctionTable, functionTable);
		definition.Set(Def_PropertyMap, properties);
		definition.Set(Def_DefinitionValidator, definitionValidatorOffset);

		IDefinitionCreated(definition);
		return definition;
	}
}

/******************************************
 *
 *   Internal helpers
 *
 ******************************************/
static bool DefaultDefinitionValidator()
{
	return true;
}

/******************************************
 *
 *   Ewww, internal workarounds :(
 *
 ******************************************/
static void DestroyDefinition(IDefinition definition)
{
	definition.Destroy();
}

static bool IsFromPlugin(IDefinition definition, Handle plugin, bool checkParents)
{
	if (definition.OwningPlugin == plugin)
	{
		return true;
	}
	if (checkParents && definition.Parent != null)
	{
		return IsFromPlugin(definition.Parent, plugin, checkParents);
	}
	return false;
}

static void AddChildrenToArray(ArrayList array, IDefinition definition, bool directDescendants, bool filterTemplates)
{
	ArrayList children = definition.Children;
	int length = children.Length;
	for (int i = 0; i < length; i++)
	{
		IDefinition child = view_as<IDefinition>(children.Get(i));
		if (child.IsComplete)
		{
			if (filterTemplates == false || child.IsTemplate == false)
			{
				array.Push(child);
			}

			if (directDescendants == false)
			{
				AddChildrenToArray(array, child, directDescendants, filterTemplates);
			}
		}
	}
}

static void TurnIncomplete(IDefinition definition)
{
	definition.TurnIncomplete();
}

static void CheckReferences(IDefinition definition)
{
	definition.CheckReferences();
}

/******************************************
 *
 *   Ewww, EXTERNAL workarounds D:
 *
 ******************************************/

void Definition_NotifyResolved(IDefinition definition)
{
	definition.ReferenceResolved();
}

void Definition_NotifyUnresolved(IDefinition definition)
{
	definition.ReferenceUnresolved();
}