#include <sourcemod>
#include <definitions>

public void OnAllPluginsLoaded()
{
	DefinitionCreator creator = new DefinitionCreator("MyTest");
	creator.DefineString("MyTestString");
	Definition test = creator.Finalize();

	if (test.IsValid())
	{
		PrintToServer("Is Template? %d(ex: 1), IsValid(template nope): %d(ex: 0)", test.IsTemplate, test.IsValid(false));

		char myTestString[16];
		if (test.GetString("MyTestString", myTestString, sizeof(myTestString)))
		{
			PrintToServer("What!? %d", myTestString);
		}
		else
		{
			PrintToServer("Cannot retrieve MyTestString! Yay!");
		}

		creator = new DefinitionCreator("MyTestChild", test);
		creator.SetString("MyTestString", "ToWin4You");
		Definition testChild = creator.Finalize();

		if (testChild.IsValidEx(false, test))
		{
			PrintToServer("Is Child Template? %d(ex: 0), IsValid(allow abstract, ignore parent): %d(ex: 1), IsValid(allow abstract, wrong parent): %d(ex: 0)", testChild.IsTemplate, testChild.IsValid(), testChild.IsValidEx(true, testChild));
			testChild.GetString("MyTestString", myTestString, sizeof(myTestString));
			PrintToServer("MyTestString (ex: 'ToWin4You'): %s", myTestString);
		}
		else
		{
			PrintToServer("MyTestChild failed to be created :(");
		}

		testChild = null;
		if (testChild.IsValid())
		{
			PrintToServer("TestChild should be null and not valid, yet we ended up here, worrying..");
		}
		else
		{
			PrintToServer("TestChild is not valid, which is correct!");
		}

		creator = new DefinitionCreator("Referencer");
		creator.SetDefinition("Ref", "TestReference");
		Definition referencer = creator.Finalize();

		creator = new DefinitionCreator("ReferencerChild", referencer);
		Definition referencerChild = creator.Finalize();

		creator = new DefinitionCreator("TestReference");
		Definition reference = creator.Finalize();

		if (referencer.GetDefinitionEx("Ref") == reference)
		{
			PrintToServer("Yay! Reference worked!");
		}
		else
		{
			PrintToServer("Crap, reference didn't work :( %x != %x", referencer.GetDefinitionEx("Ref"), reference);
		}

		PrintToServer("IsComplete: referencer: %d(ex: 1), child: %d(ex: 1)", referencer.IsComplete, referencerChild.IsComplete);
		reference.Destroy();
		PrintToServer("IsComplete: referencer: %d(ex: 0), child: %d(ex: 0)", referencer.IsComplete, referencerChild.IsComplete);

		creator = new DefinitionCreator("ReferencerChild2", referencer);
		Definition referencerChild2 = creator.Finalize();
		creator = new DefinitionCreator("ReferencerChild3", referencer);
		creator.SetDefinition("Ref", "nyeref");
		Definition referencerChild3 = creator.Finalize();
		PrintToServer("IsComplete: referencer: %d(ex: 0), child: %d(ex: 0), child2: %d(ex: 0), child3: %d(ex: 0)", referencer.IsComplete, referencerChild.IsComplete, referencerChild2.IsComplete, referencerChild3.IsComplete);

		creator = new DefinitionCreator("TestReference");
		reference = creator.Finalize();
		PrintToServer("IsComplete: referencer: %d(ex: 1), child: %d(ex: 1), child2: %d(ex: 1), child3: %d(ex: 0)", referencer.IsComplete, referencerChild.IsComplete, referencerChild2.IsComplete, referencerChild3.IsComplete);


		creator = new DefinitionCreator("nyeref");
		creator.Finalize();
		PrintToServer("IsComplete: referencer: %d(ex: 1), child: %d(ex: 1), child2: %d(ex: 1), child3: %d(ex: 1)", referencer.IsComplete, referencerChild.IsComplete, referencerChild2.IsComplete, referencerChild3.IsComplete);
	}
	else
	{
		PrintToServer("MyTest failed to be created :(");
	}

	DestroyMyDefinitions();
}

public void OnDefinitionCreated(Definition definition)
{
	PrintDefinitionMessage("Created:", definition);
}

public void OnDefinitionDestroy(Definition definition)
{
	PrintDefinitionMessage("Destroy:", definition);
}

public void OnDefinitionDestroyed(Definition definition)
{
	PrintDefinitionMessage("Destroyed:", definition);
}

void PrintDefinitionMessage(char[] message, Definition definition)
{
	int nameLength = definition.NameLength + 1;
	char[] name = new char[nameLength];
	definition.GetName(name, nameLength);

	PrintToServer("%s %s", message, name);
}